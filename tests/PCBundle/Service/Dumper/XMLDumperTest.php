<?php

namespace Tests\PCBundle\Service\Dumper;

use PHPUnit\Framework\TestCase;
use Zechim\PCBundle\Service\Dumper\XMLDumper;
use Zechim\PCBundle\Service\Element\ElementCollection;

class XMLDumperTest extends TestCase
{
    public function testShouldCreateCollection()
    {
        $collection = new ElementCollection(require __DIR__ . '/../Element/configuration_1.xml.php');
        $value = $collection->fill(require __DIR__ . '/../Element/full_request_data.php');

        $dumper = new XMLDumper(['formatOutput' => true]);
        $xml = $dumper->dump($value);

        $this->assertEquals(require 'dumped.xml.php', $xml);
    }
}