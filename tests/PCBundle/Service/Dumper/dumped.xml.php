<?php

return '<?xml version="1.0" encoding="utf-8"?>
<root xmlns:root_attr_1="root_my_value">
  <root_1 root_1_my_attr_1="filled">root_1_my_value</root_1>
  <root_2 root_2_my_attr_1="root_2_my_value_1" prefix:root_2_my_attr_2="root_2_my_value_2" root_2_my_attr_2="root_2_my_value_3">
    <root_2_1>root_2_1_my_value</root_2_1>
    <root_2_2>root_2_2_my_value</root_2_2>
    <root_2_3>
      <root_2_3_1>root_2_3_1_my_value_1</root_2_3_1>
      <root_2_3_1>root_2_3_1_my_value_2</root_2_3_1>
      <root_2_3_1>root_2_3_1_my_value_3</root_2_3_1>
    </root_2_3>
    <root_2_4 root_2_4_my_attr_1="value">
      <root_2_4_1>root_2_4_1_my_value_1</root_2_4_1>
    </root_2_4>
    <prefix:root_2_5 root_2_5_my_attr_1="value">
      <root_2_5_1 root_2_5_1_my_attr_1="value">
        <root_2_5_1_1>root_2_5_1_1_my_value_1_1</root_2_5_1_1>
        <root_2_5_1_1>root_2_5_1_1_my_value_1_2</root_2_5_1_1>
      </root_2_5_1>
      <root_2_5_1 root_2_5_1_my_attr_1="value">
        <root_2_5_1_1>root_2_5_1_1_my_value_2_1</root_2_5_1_1>
        <root_2_5_1_1>root_2_5_1_1_my_value_2_2</root_2_5_1_1>
        <root_2_5_1_1>root_2_5_1_1_my_value_2_3</root_2_5_1_1>
      </root_2_5_1>
    </prefix:root_2_5>
  </root_2>
</root>
';