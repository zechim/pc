<?php

namespace Tests\PCBundle\Service\Dumper;

use PHPUnit\Framework\TestCase;
use Zechim\PCBundle\Service\Element\AttributeCollection;
use Zechim\PCBundle\Service\Tag\Tag;
use Zechim\PCBundle\Service\Tag\TagCollection;

class TagCollectionTest extends TestCase
{
    public function testShouldCreateCollection()
    {
        $tag1_1 = new Tag(['name' => 'tag1_1'], 'value1_1', new AttributeCollection());
        $tag1 = new Tag(['name' => 'tag1'], $tag1_1, new AttributeCollection());

        $tag2_1 = new Tag(['name' => 'tag2_1'], 'value2_1', new AttributeCollection());
        $tag2_2 = new Tag(['name' => 'tag2_2'], 'value2_2', new AttributeCollection());

        $collection = new TagCollection();
        $collection->addTag($tag2_1);
        $collection->addTag($tag2_2);

        $tag2 = new Tag(['name' => 'tag2'], $collection, new AttributeCollection());

        $collection = new TagCollection();
        $collection->addTag($tag1);
        $collection->addTag($tag2);

        $_tag1 = $collection->current();
        $_tag1_1 = $_tag1->current();

        $collection->next();

        $_tag2 = $collection->current();
        $_tag2_collection = $_tag2->current();
        $_tag2_1 = $_tag2_collection->current();
        $_tag2_collection->next();
        $_tag2_2 = $_tag2_collection->current();

        $this->assertCount(2, $collection);

        $this->assertSame($tag1, $_tag1);
        $this->assertEquals('tag1', $_tag1->getName());
        $this->assertCount(1, $_tag1);

        $this->assertSame($tag1_1, $_tag1_1);
        $this->assertEquals('tag1_1', $_tag1_1->getName());
        $this->assertEquals('value1_1', $_tag1_1->getValue());
        $this->assertCount(0, $_tag1_1);

        $this->assertSame($tag2, $_tag2);
        $this->assertEquals('tag2', $_tag2->getName());
        $this->assertCount(1, $_tag2);

        $this->assertCount(2, $_tag2_collection);

        $this->assertSame($tag2_1, $_tag2_1);
        $this->assertEquals('tag2_1', $_tag2_1->getName());
        $this->assertEquals('value2_1', $_tag2_1->getValue());
        $this->assertCount(0, $_tag2_1);

        $this->assertSame($tag2_2, $_tag2_2);
        $this->assertEquals('tag2_2', $_tag2_2->getName());
        $this->assertEquals('value2_2', $_tag2_2->getValue());
        $this->assertCount(0, $_tag2_2);
    }
}