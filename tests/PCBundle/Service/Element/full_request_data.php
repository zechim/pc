<?php

if (false === function_exists('createItem')) {
    function createItem($value, $attributes = [])
    {
        if (count($attributes)) {
            return ['$' => $value, '@' => $attributes];

        } else {
            return ['$' => $value];
        }
    }
}

$root = createItem([
    'root_1' => createItem('root_1_my_value', ['root_1_my_attr_1' => 'filled']),
    'root_2' => createItem([
        'root_2_1' => createItem('root_2_1_my_value'),
        'root_2_2' => createItem('root_2_2_my_value'),
        'root_2_3' => createItem([
            ['root_2_3_1' => createItem('root_2_3_1_my_value_1')],
            ['root_2_3_1' => createItem('root_2_3_1_my_value_2')],
            ['root_2_3_1' => createItem('root_2_3_1_my_value_3')],
        ]),
        'root_2_4' => createItem([
            'root_2_4_1' => createItem('root_2_4_1_my_value_1'),
        ]),
        'root_2_5' => createItem([
            [
                'root_2_5_1' => createItem([
                    ['root_2_5_1_1' => createItem('root_2_5_1_1_my_value_1_1')],
                    ['root_2_5_1_1' => createItem('root_2_5_1_1_my_value_1_2')],
                ])
            ],
            [
                'root_2_5_1' => createItem([
                    ['root_2_5_1_1' => createItem('root_2_5_1_1_my_value_2_1')],
                    ['root_2_5_1_1' => createItem('root_2_5_1_1_my_value_2_2')],
                    ['root_2_5_1_1' => createItem('root_2_5_1_1_my_value_2_3')],
                ])
            ],
        ])
    ]),
]);

return ['$' => ['root' => $root]];