<?php

namespace Tests\PCBundle\Service\Element;

use PHPUnit\Framework\TestCase;
use Zechim\PCBundle\Service\Element\ArrayElement;
use Zechim\PCBundle\Service\Element\ElementCollection;
use Zechim\PCBundle\Service\Element\MultipleElement;
use Zechim\PCBundle\Service\Element\ValueElement;

class ElementCollectionTest extends TestCase
{
    public function testShouldCreateFullCollection()
    {
        $collection = new ElementCollection(require 'configuration_1.xml.php');
        $this->assertCount(1, $collection);

        $root = $collection->current();
        $this->assertInstanceOf(ArrayElement::class, $root);
        $this->assertEquals('root', $root->getName());
        $this->assertEquals('root', $root->getPath());
        $this->assertSame($collection, $root->getParent());
        $this->assertCount(2, $root);

        $root_1 = $root->current();
        $this->assertInstanceOf(ValueElement::class, $root_1);
        $this->assertEquals('root_1', $root_1->getName());
        $this->assertEquals('root.root_1', $root_1->getPath());
        $this->assertSame($root, $root_1->getParent());
        $this->assertCount(0, $root_1);

        $root->next();

        $root_2 = $root->current();
        $this->assertInstanceOf(ArrayElement::class, $root_2);
        $this->assertEquals('root_2', $root_2->getName());
        $this->assertEquals('root.root_2', $root_2->getPath());
        $this->assertSame($root, $root_2->getParent());
        $this->assertCount(5, $root_2);

        $root_2_1 = $root_2->current();
        $this->assertInstanceOf(ValueElement::class, $root_2_1);
        $this->assertEquals('root_2_1', $root_2_1->getName());
        $this->assertEquals('root.root_2.root_2_1', $root_2_1->getPath());
        $this->assertSame($root_2, $root_2_1->getParent());
        $this->assertCount(0, $root_2_1);

        $root_2->next();

        $root_2_2 = $root_2->current();
        $this->assertInstanceOf(ValueElement::class, $root_2_2);
        $this->assertEquals('root_2_2', $root_2_2->getName());
        $this->assertEquals('root.root_2.root_2_2', $root_2_2->getPath());
        $this->assertSame($root_2, $root_2_2->getParent());
        $this->assertCount(0, $root_2_2);

        $root_2->next();

        $root_2_3 = $root_2->current();
        $this->assertInstanceOf(MultipleElement::class, $root_2_3);
        $this->assertEquals('root_2_3', $root_2_3->getName());
        $this->assertEquals('root.root_2.root_2_3', $root_2_3->getPath());
        $this->assertSame($root_2, $root_2_3->getParent());
        $this->assertCount(1, $root_2_3);

        $root_2_3_1 = $root_2_3->current();
        $this->assertInstanceOf(ValueElement::class, $root_2_3_1);
        $this->assertEquals('root_2_3_1', $root_2_3_1->getName());
        $this->assertEquals('root.root_2.root_2_3.root_2_3_1', $root_2_3_1->getPath());
        $this->assertSame($root_2_3, $root_2_3_1->getParent());
        $this->assertCount(0, $root_2_3_1);

        $root_2->next();

        $root_2_4 = $root_2->current();
        $this->assertInstanceOf(ArrayElement::class, $root_2_4);
        $this->assertEquals('root_2_4', $root_2_4->getName());
        $this->assertEquals('root.root_2.root_2_4', $root_2_4->getPath());
        $this->assertSame($root_2, $root_2_4->getParent());
        $this->assertCount(1, $root_2_4);

        $root_2_4_1 = $root_2_4->current();
        $this->assertInstanceOf(ValueElement::class, $root_2_4_1);
        $this->assertEquals('root_2_4_1', $root_2_4_1->getName());
        $this->assertEquals('root.root_2.root_2_4.root_2_4_1', $root_2_4_1->getPath());
        $this->assertSame($root_2_4, $root_2_4_1->getParent());
        $this->assertCount(0, $root_2_4_1);

        $root_2->next();

        $root_2_5 = $root_2->current();
        $this->assertInstanceOf(MultipleElement::class, $root_2_5);
        $this->assertEquals('root_2_5', $root_2_5->getName());
        $this->assertEquals('root.root_2.root_2_5', $root_2_5->getPath());
        $this->assertSame($root_2, $root_2_5->getParent());
        $this->assertCount(1, $root_2_5);

        $root_2_5_1 = $root_2_5->current();
        $this->assertInstanceOf(MultipleElement::class, $root_2_5_1);
        $this->assertEquals('root_2_5_1', $root_2_5_1->getName());
        $this->assertEquals('root.root_2.root_2_5.root_2_5_1', $root_2_5_1->getPath());
        $this->assertSame($root_2_5, $root_2_5_1->getParent());
        $this->assertCount(1, $root_2_5_1);

        $root_2_5_1_1 = $root_2_5_1->current();
        $this->assertInstanceOf(ValueElement::class, $root_2_5_1_1);
        $this->assertEquals('root_2_5_1_1', $root_2_5_1_1->getName());
        $this->assertEquals('root.root_2.root_2_5.root_2_5_1.root_2_5_1_1', $root_2_5_1_1->getPath());
        $this->assertSame($root_2_5_1, $root_2_5_1_1->getParent());
        $this->assertCount(0, $root_2_5_1_1);

        $root_2_5_1_1->next();
        $this->assertFalse($root_2_5_1_1->current());

        $root_2_5->next();
        $this->assertFalse($root_2_5->current());

        $root_2->next();
        $this->assertFalse($root_2->current());
    }

    public function testFillTagCollectionBasedOnRequestData()
    {
        $collection = new ElementCollection(require 'configuration_1.xml.php');
        $value = $collection->fill(require 'full_request_data.php');

        $root = $value->current();

        $root_1 = $root->current();
        $root->next();
        $root_2 = $root->current();

        $root_2_1 = $root_2->current();
        $root_2->next();
        $root_2_2 = $root_2->current();
        $root_2->next();
        $root_2_3 = $root_2->current();
        $root_2->next();
        $root_2_4 = $root_2->current();
        $root_2->next();
        $root_2_5 = $root_2->current();

        $root_2_3_1_a = $root_2_3->current();
        $root_2_3->next();
        $root_2_3_1_b = $root_2_3->current();
        $root_2_3->next();
        $root_2_3_1_c = $root_2_3->current();

        $root_2_4_1 = $root_2_4->current();

        $root_2_5_1_a = $root_2_5->current();
        $root_2_5->next();
        $root_2_5_1_b = $root_2_5->current();

        $root_2_5_1_1_a_1 = $root_2_5_1_a->current();
        $root_2_5_1_a->next();
        $root_2_5_1_1_a_2 = $root_2_5_1_a->current();
        $root_2_5_1_a->next();

        $root_2_5_1_1_b_1 = $root_2_5_1_b->current();
        $root_2_5_1_b->next();
        $root_2_5_1_1_b_2 = $root_2_5_1_b->current();
        $root_2_5_1_b->next();
        $root_2_5_1_1_b_3 = $root_2_5_1_b->current();

        $this->assertEquals('root', $root->getName());
        $this->assertEquals('root_1', $root_1->getName());
        $this->assertEquals('root_1_my_value', $root_1->getValue());

        $this->assertEquals('root_2_1', $root_2_1->getName());
        $this->assertEquals('root_2_1_my_value', $root_2_1->getValue());

        $this->assertEquals('root_2_2', $root_2_2->getName());
        $this->assertEquals('root_2_2_my_value', $root_2_2->getValue());

        $this->assertEquals('root_2_3_1_my_value_1', $root_2_3_1_a->getValue());
        $this->assertEquals('root_2_3_1', $root_2_3_1_b->getName());
        $this->assertEquals('root_2_3_1_my_value_2', $root_2_3_1_b->getValue());
        $this->assertEquals('root_2_3_1', $root_2_3_1_c->getName());
        $this->assertEquals('root_2_3_1_my_value_3', $root_2_3_1_c->getValue());

        $this->assertEquals('root_2_4_1', $root_2_4_1->getName());
        $this->assertEquals('root_2_4_1_my_value_1', $root_2_4_1->getValue());

        $this->assertEquals('root_2_5_1_1', $root_2_5_1_1_a_1->getName());
        $this->assertEquals('root_2_5_1_1_my_value_1_1', $root_2_5_1_1_a_1->getValue());

        $this->assertEquals('root_2_5_1_1', $root_2_5_1_1_a_2->getName());
        $this->assertEquals('root_2_5_1_1_my_value_1_2', $root_2_5_1_1_a_2->getValue());

        $this->assertEquals('root_2_5_1_1', $root_2_5_1_1_b_1->getName());
        $this->assertEquals('root_2_5_1_1_my_value_2_1', $root_2_5_1_1_b_1->getValue());

        $this->assertEquals('root_2_5_1_1', $root_2_5_1_1_b_1->getName());
        $this->assertEquals('root_2_5_1_1_my_value_2_2', $root_2_5_1_1_b_2->getValue());

        $this->assertEquals('root_2_5_1_1', $root_2_5_1_1_b_1->getName());
        $this->assertEquals('root_2_5_1_1_my_value_2_3', $root_2_5_1_1_b_3->getValue());
    }

    /**
     * @expectedException \OutOfBoundsException
     * @expectedExceptionMessage key root required, but "x, @" was fou
     */
    public function testShouldCheckRuleRequired()
    {
        $collection = new ElementCollection('<configuration>
            <element>
                <name>root</name>
                <type>array</type>
                <rules><required>true</required></rules>
            </element>
        </configuration>');

        $collection->fill(['$' => ['x' => ['$']]]);

        $this->assertTrue(true);
    }
}