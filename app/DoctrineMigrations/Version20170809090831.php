<?php

namespace Application\Migrations;

use Zechim\AppBundle\Doctrine\DBAL\Migrations\AbstractAppMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170809090831 extends AbstractAppMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $icon = 'wrench';
        $parent = 'configuration';
        
        $this->createRoute('zechim_dispatcher', 'user_permission', 'index', $icon, $parent, true);
        $this->createRoute('zechim_dispatcher', 'user_permission', 'show', null, 'user_permission');
        $this->createRoute('zechim_dispatcher', 'user_permission', 'create', null, 'user_permission');
        $this->createRoute('zechim_dispatcher', 'user_permission', 'edit', null, 'user_permission');
        $this->createRoute('zechim_dispatcher', 'user_permission', 'delete', null, 'user_permission');

        $this->createRouteUp();
    }

    /**
     * @param Schema $schema
     */ 
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->deleteRoute('zechim_dispatcher', 'user_permission', 'delete');
        $this->deleteRoute('zechim_dispatcher', 'user_permission', 'edit');
        $this->deleteRoute('zechim_dispatcher', 'user_permission', 'create');
        $this->deleteRoute('zechim_dispatcher', 'user_permission', 'show');
        $this->deleteRoute('zechim_dispatcher', 'user_permission', 'index');
    }
}
