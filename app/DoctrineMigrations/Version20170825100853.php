<?php

namespace Application\Migrations;

use Zechim\AppBundle\Doctrine\DBAL\Migrations\AbstractAppMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170825100853 extends AbstractAppMigration
{
    protected $prefix = 'zechim_api';
    
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $icon = 'credit-card';
        $parent = 'configuration';
        
        $this->createRoute($this->prefix, 'user_token', 'index', $icon, $parent, true);
        $this->createRoute($this->prefix, 'user_token', 'show');
        $this->createRoute($this->prefix, 'user_token', 'create');
        $this->createRoute($this->prefix, 'user_token', 'edit');
        $this->createRoute($this->prefix, 'user_token', 'delete');

        $this->createRouteUp();
    }

    /**
     * @param Schema $schema
     */ 
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->deleteRoute($this->prefix, 'user_token', 'delete');
        $this->deleteRoute($this->prefix, 'user_token', 'edit');
        $this->deleteRoute($this->prefix, 'user_token', 'create');
        $this->deleteRoute($this->prefix, 'user_token', 'show');
        $this->deleteRoute($this->prefix, 'user_token', 'index');
    }
}
