<?php

namespace Application\Migrations;

use Zechim\AppBundle\Doctrine\DBAL\Migrations\AbstractAppMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170825100854 extends AbstractAppMigration
{
    protected $prefix = 'zechim_pc_api';
    
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->createRoute($this->prefix, 'request', 'create');

        $this->createRouteUp();
    }

    /**
     * @param Schema $schema
     */ 
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->deleteRoute($this->prefix, 'request', 'create');
    }
}
