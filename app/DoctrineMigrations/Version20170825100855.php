<?php

namespace Application\Migrations;

use Zechim\AppBundle\Doctrine\DBAL\Migrations\AbstractAppMigration;
use Doctrine\DBAL\Schema\Schema;
use Zechim\PCBundle\Entity\Request;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170825100855 extends AbstractAppMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->createTypeUp(Request::STATUS_TYPE_AWAITING, 'AWAITING');
        $this->createTypeUp(Request::STATUS_TYPE_DUMPING, 'DUMPING');
        $this->createTypeUp(Request::STATUS_TYPE_READY, 'READY');
        $this->createTypeUp(Request::STATUS_TYPE_ERROR, 'ERROR');
    }

    /**
     * @param Schema $schema
     */ 
    public function down(Schema $schema)
    {
        $this->createTypeDown(Request::STATUS_TYPE_AWAITING);
        $this->createTypeDown(Request::STATUS_TYPE_DUMPING);
        $this->createTypeDown(Request::STATUS_TYPE_READY);
        $this->createTypeDown(Request::STATUS_TYPE_ERROR);
    }
}
