<?php

namespace Application\Migrations;

use Zechim\AppBundle\Doctrine\DBAL\Migrations\AbstractAppMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170809090829 extends AbstractAppMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $icon = 'user';
        $parent = 'configuration';
        
        $this->createRoute('zechim_dispatcher', 'user', 'index', $icon, $parent, true);
        $this->createRoute('zechim_dispatcher', 'user', 'show', null, 'user');
        $this->createRoute('zechim_dispatcher', 'user', 'create', null, 'user');
        $this->createRoute('zechim_dispatcher', 'user', 'edit', null, 'user');
        $this->createRoute('zechim_dispatcher', 'user', 'delete', null, 'user');

        $this->createRouteUp();
    }

    /**
     * @param Schema $schema
     */ 
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->deleteRoute('zechim_dispatcher', 'user', 'delete');
        $this->deleteRoute('zechim_dispatcher', 'user', 'edit');
        $this->deleteRoute('zechim_dispatcher', 'user', 'create');
        $this->deleteRoute('zechim_dispatcher', 'user', 'show');
        $this->deleteRoute('zechim_dispatcher', 'user', 'index');
    }
}
