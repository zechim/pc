<?php

namespace Application\Migrations;

use Doctrine\ORM\EntityManager;
use Zechim\AppBundle\Doctrine\DBAL\Migrations\AbstractAppMigration;
use Doctrine\DBAL\Schema\Schema;
use Zechim\AppBundle\Entity\User;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170413141734 extends AbstractAppMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        /** @var EntityManager $em */
        $em = $this->container->get('doctrine.orm.entity_manager');
        $user = $em->getRepository(User::class)->findOneBy(['username' => 'zechim']);

        $user = null === $user ? new User() : $user;
        $user->setActive(true);
        $user->setName('zechim');
        $user->setUsername('zechim');
        $user->setPassword($this->container->get('security.password_encoder')->encodePassword($user, 'zechim'));

        $em->persist($user);
        $em->flush($user);

        $user->setUpdatedBy($user);
        $user->setCreatedBy($user);

        $em->persist($user);
        $em->flush($user);

        # configuration
        $this->createRoute('zechim_dispatcher', 'configuration', null, 'cog', null, true, 9999);

        $this->addSql("INSERT INTO `user_permission` (`name`, `editable`, `admin`) VALUES ('ADMIN', 0, 1)");
        $this->addSql("INSERT INTO `user_has_user_permission` VALUES ((SELECT `id` FROM `user` WHERE `username` = 'zechim'), (SELECT id FROM `user_permission` WHERE name = 'ADMIN'))");

        $this->createRouteUp();
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql("DELETE FROM `user_has_user_permission`");
        $this->addSql("DELETE FROM `user_permission_has_user_action`");
        $this->addSql("DELETE FROM `user_permission`");

        $this->addSql("UPDATE `menu` SET `parent_menu_id` = NULL");
        $this->addSql("DELETE FROM `menu`");
        $this->addSql("DELETE FROM `user_action`");
        $this->addSql("DELETE FROM `user_permission`");
        $this->addSql("UPDATE `user` SET `update_user_id` = NULL, `create_user_id` = NULL");
        $this->addSql("DELETE FROM `user`");
    }
}
