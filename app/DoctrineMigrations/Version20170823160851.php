<?php

namespace Application\Migrations;

use Zechim\AppBundle\Doctrine\DBAL\Migrations\AbstractAppMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170823160851 extends AbstractAppMigration
{
    protected $prefix = 'zechim_pc';
    
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $icon = 'road';
        $parent = null;
        
        $this->createRoute($this->prefix, 'configuration', 'index', $icon, $parent, true);
        $this->createRoute($this->prefix, 'configuration', 'show');
        $this->createRoute($this->prefix, 'configuration', 'create');
        $this->createRoute($this->prefix, 'configuration', 'edit');
        $this->createRoute($this->prefix, 'configuration', 'delete');

        $this->createRouteUp();
    }

    /**
     * @param Schema $schema
     */ 
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->deleteRoute($this->prefix, 'configuration', 'delete');
        $this->deleteRoute($this->prefix, 'configuration', 'edit');
        $this->deleteRoute($this->prefix, 'configuration', 'create');
        $this->deleteRoute($this->prefix, 'configuration', 'show');
        $this->deleteRoute($this->prefix, 'configuration', 'index');
    }
}
