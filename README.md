## Install

Please check `app/config/parameters.yml.dist` before database operations

* create application database
```
bin/console doctrine:database:create
```

* create application schema
```
bin/console doctrine:schema:create
```

* run migrations 
```
bin/console doctrine:migrations:migrate
```

* you need ruby and bunny (gem)

* set up consumer to listen to AMQ broker

`ROUTE_KEY=my_route_key`

`EXCHANGE_KEY=my_exchange_key`

`./vendor/bin/consumer -r $ROUTE_KEY -x $EXCHANGE_KEY -c bin/console -env prod -p path/to/pid.id`

# Update

* update application schema
```
bin/console doctrine:schema:update --force
```

* run migrations
```
bin/console doctrine:migrations:migrate
```

## API (create request)

POST `api/request/create/{configuration}`

HEADER `app-token: TOKEN`

BODY `json encoded data`