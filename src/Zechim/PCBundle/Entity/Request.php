<?php

namespace Zechim\PCBundle\Entity;

use Zechim\AppBundle\Entity\Type;
use Zechim\AppBundle\Entity\User;

/**
 * Request
 */
class Request
{
    const STATUS_TYPE_AWAITING = 'REQUEST_STATUS_TYPE_AWAITING';
    const STATUS_TYPE_DUMPING = 'REQUEST_STATUS_TYPE_DUMPING';
    const STATUS_TYPE_READY = 'REQUEST_STATUS_TYPE_READY';
    const STATUS_TYPE_ERROR = 'REQUEST_STATUS_TYPE_ERROR';

    /**
     * @var integer
     */
    private $id;

    /**
     * @var array
     */
    private $input;

    /**
     * @var string
     */
    private $output;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * @var Configuration
     */
    private $configuration;

    /**
     * @var Type
     */
    private $statusType;

    /**
     * @var User
     */
    private $createdBy;

    /**
     * @var User
     */
    private $updatedBy;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set input
     *
     * @param array $input
     *
     * @return Request
     */
    public function setInput($input)
    {
        $this->input = $input;

        return $this;
    }

    /**
     * Get input
     *
     * @return array
     */
    public function getInput()
    {
        return $this->input;
    }

    /**
     * Set output
     *
     * @param string $output
     *
     * @return Request
     */
    public function setOutput($output)
    {
        $this->output = $output;

        return $this;
    }

    /**
     * Get output
     *
     * @return string
     */
    public function getOutput()
    {
        return $this->output;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Request
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Request
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set configuration
     *
     * @param Configuration $configuration
     *
     * @return Request
     */
    public function setConfiguration(Configuration $configuration = null)
    {
        $this->configuration = $configuration;

        return $this;
    }

    /**
     * Get configuration
     *
     * @return Configuration
     */
    public function getConfiguration()
    {
        return $this->configuration;
    }

    /**
     * Set statusType
     *
     * @param Type $statusType
     *
     * @return Request
     */
    public function setStatusType(Type $statusType = null)
    {
        $this->statusType = $statusType;

        return $this;
    }

    /**
     * Get statusType
     *
     * @return Type
     */
    public function getStatusType()
    {
        return $this->statusType;
    }

    /**
     * Set createdBy
     *
     * @param User $createdBy
     *
     * @return Request
     */
    public function setCreatedBy(User $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set updatedBy
     *
     * @param User $updatedBy
     *
     * @return Request
     */
    public function setUpdatedBy(User $updatedBy = null)
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    /**
     * Get updatedBy
     *
     * @return User
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }
}
