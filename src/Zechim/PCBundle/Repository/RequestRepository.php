<?php

namespace Zechim\PCBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Zechim\AppBundle\Entity\Type;
use Zechim\AppBundle\Entity\User;
use Zechim\PCBundle\Entity\Configuration;
use Zechim\PCBundle\Entity\Request;

class RequestRepository extends EntityRepository
{
    public function create(Configuration $configuration, array $input, User $user)
    {
        $statusType = $this->_em->getRepository(Type::class)
            ->findOneByCode(Request::STATUS_TYPE_AWAITING);

        $request = new Request();
        $request->setCreatedAt(new \DateTime);
        $request->setUpdatedAt(new \DateTime);
        $request->setConfiguration($configuration);
        $request->setInput($input);
        $request->setCreatedBy($user);
        $request->setStatusType($statusType);

        $this->_em->persist($request);
        $this->_em->flush($request);

        return $request;
    }

    public function canTransform(Request $request)
    {
        return true === in_array(
            $request->getStatusType()->getCode(),
            [
                Request::STATUS_TYPE_AWAITING
            ]
        );
    }

    public function setDumping(Request $request)
    {
        $statusType = $this->_em->getRepository(Type::class)
            ->findOneByCode(Request::STATUS_TYPE_DUMPING);

        $request->setUpdatedAt(new \DateTime);
        $request->setStatusType($statusType);

        $this->_em->persist($request);
        $this->_em->flush($request);

        return $request;
    }

    public function setError(Request $request, $message)
    {
        $statusType = $this->_em->getRepository(Type::class)
            ->findOneByCode(Request::STATUS_TYPE_ERROR);

        $request->setUpdatedAt(new \DateTime);
        $request->setStatusType($statusType);
        $request->setOutput($message);

        $this->_em->persist($request);
        $this->_em->flush($request);

        return $request;
    }

    public function setOutput(Request $request, $output)
    {
        $statusType = $this->_em->getRepository(Type::class)
            ->findOneByCode(Request::STATUS_TYPE_READY);

        $request->setUpdatedAt(new \DateTime);
        $request->setStatusType($statusType);
        $request->setOutput($output);

        $this->_em->persist($request);
        $this->_em->flush($request);

        return $request;

    }

}
