<?php

namespace Zechim\PCBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Security\Core\User\UserInterface;
use Zechim\PCBundle\Entity\Configuration;

class ConfigurationRepository extends EntityRepository
{
    public function create(Configuration $configuration, UserInterface $user)
    {
        $configuration->setCreatedBy($user);
        $configuration->setUpdatedBy($user);
        $configuration->setCreatedAt(new \DateTime);
        $configuration->setUpdatedAt(new \DateTime);

        $this->_em->persist($configuration);
        $this->_em->flush($configuration);

        return $configuration;
    }
};
