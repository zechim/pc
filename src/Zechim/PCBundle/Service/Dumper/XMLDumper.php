<?php

namespace Zechim\PCBundle\Service\Dumper;

use Zechim\PCBundle\Service\Element\Attribute;
use Zechim\PCBundle\Service\Tag\Tag;
use Zechim\PCBundle\Service\Tag\TagCollection;

class XMLDumper
{
    /**
     * @var $options
     */
    protected $options;

    public function __construct(array $options = [])
    {
        $this->options = array_merge(['formatOutput' => false], $options);
    }

    public function dump(TagCollection $collection)
    {
        $document = new \DOMDocument('1.0', 'utf-8');
        $document->formatOutput = $this->options['formatOutput'];

        /** @var Tag $tag */
        foreach ($collection as $tag) {
            $node = $this->createNode($document, $tag);

            $document->appendChild($node);

            foreach ($tag as $child) {
                $this->createNodes($document, $node, $child);
            }
        }

        return $document->saveXML();
    }

    protected function createNodes(\DOMDocument $document, \DOMElement $element, TagCollection $collection)
    {
        /** @var \Zechim\PCBundle\Service\Tag\Tag $collection */
        if (true === $collection instanceof Tag) {
            $element->appendChild($element = $this->createNode($document, $collection));
        }

        /** @var \Zechim\PCBundle\Service\Tag\Tag $tag */
        foreach ($collection as $tag) {
            $node = $this->createNode($document, $tag);

            $element->appendChild($node);

            foreach ($tag as $child) {
                $this->createNodes($document, $node, $child);
            }
        }
    }

    /**
     * @param \DOMDocument $document
     * @param Tag $tag
     * @return \DOMElement
     */
    protected function createNode(\DOMDocument $document, Tag $tag)
    {
        if ('' !== $tag->getNamespace()) {
            $node = $document->createElementNS($tag->getNamespace(), $tag->getName(true), $tag->getValue());

        } else {
            $node = $document->createElement($tag->getName(true), $tag->getValue());
        }

        $this->createAttributes($document, $node, $tag);

        return $node;
    }

    protected function createAttributes(\DOMDocument $document, \DOMElement $node, Tag $tag)
    {
        /** @var Attribute $attribute */
        foreach ($tag->getAttributes() as $attribute) {
            if ('' !== $attribute->getNamespace()) {
                $node->setAttributeNS($attribute->getNamespace(), $attribute->getName(true), $attribute->getValue());

            } else {

                $node->setAttribute($attribute->getName(true), $attribute->getValue());
            }
        }
    }

}