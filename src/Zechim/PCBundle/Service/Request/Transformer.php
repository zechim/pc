<?php

namespace Zechim\PCBundle\Service\Request;

use Zechim\PCBundle\Entity\Request;
use Zechim\PCBundle\Repository\RequestRepository;
use Zechim\PCBundle\Service\Dumper\XMLDumper;
use Zechim\PCBundle\Service\Element\ElementCollection;

class Transformer
{
    /**
     * @var RequestRepository
     */
    protected $repository;

    public function __construct(RequestRepository $repository)
    {
        $this->repository = $repository;
    }

    public function transform(Request $request)
    {
        try {
            $this->repository->setOutput($this->getTransformationResult($request));

        } catch (\Exception $e) {
            $this->repository->setError($request, $e->getMessage());
        }
    }

    protected function getTransformationResult(Request $request)
    {
        $collection = new ElementCollection($request->getConfiguration()->getRules());

        return (new XMLDumper())->dump($collection->fill($request->getInput()));
    }
}