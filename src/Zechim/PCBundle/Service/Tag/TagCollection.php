<?php

namespace Zechim\PCBundle\Service\Tag;

class TagCollection implements \Iterator, \Countable
{
    /**
     * @var array|Tag|TagCollection
     */
    protected $tags = [];

    public function next()
    {
        return next($this->tags);
    }

    public function key()
    {
        return key($this->tags);
    }

    public function valid()
    {
        return false !== $this->current();
    }

    public function rewind()
    {
        return reset($this->tags);
    }

    public function count()
    {
        return count($this->tags);
    }

    public function current()
    {
        return current($this->tags);
    }

    public function addTag(TagCollection $tag)
    {
        $this->tags[] = $tag;
    }
}