<?php

namespace Zechim\PCBundle\Service\Tag;

use Zechim\PCBundle\Service\Element\AttributeCollection;

class Tag extends TagCollection
{
    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $namespace;

    /**
     * @var string
     */
    protected $prefix;

    /**
     * @var mixed
     */
    protected $value;

    /**
     * @var AttributeCollection
     */
    protected $attributes;

    public function __construct(array $options, $value = null, AttributeCollection $attributes)
    {
        $this->name = $options['name'];
        $this->namespace = false === empty($options['namespace']) ? $options['namespace'] : '';
        $this->prefix = false === empty($options['prefix']) ? $options['prefix'] : '';

        if ($value instanceof TagCollection) {
            $this->addTag($value);

        } else {
            $this->value = $value;
        }

        $this->attributes = $attributes;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @return string
     */
    public function getName($withPrefix = false)
    {
        if (true === $withPrefix && '' !== $this->prefix) {
            return sprintf('%s:%s', $this->prefix, $this->name);
        }

        return $this->name;
    }

    /**
     * @return string
     */
    public function getNamespace()
    {
        return $this->namespace;
    }

    public function getAttributes()
    {
        return $this->attributes;
    }
}