<?php

namespace Zechim\PCBundle\Service\Element;

use Zechim\PCBundle\Service\Tag\Tag;

class MultipleElement extends AbstractElement
{
    public function fill(array $data)
    {
        $this->check($data);

        $data = $data['$'][$this->getName()];
        $data = $this->createDefault($data, ['@'], []);

        $element = $this->current();

        $tag = new Tag(
            [
                'name' => $this->getName(),
                'namespace' => $this->getNamespace(),
                'prefix' => $this->getPrefix(),
            ],
            null,
            $this->attributes->fill($data['@'])
        );

        $data = $this->createDefault($data, ['$'], []);
        $data = $data['$'];

        foreach ($data as $item) {
            $tag->addTag($element->fill(['$' => $item]));
        }

        return $tag;
    }
}