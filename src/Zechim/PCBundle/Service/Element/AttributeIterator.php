<?php

namespace Zechim\PCBundle\Service\Element;

class AttributeIterator extends ElementIterator
{
    /**
     * @return bool|Attribute
     */
    public function current()
    {
        $current = current($this->elements);

        if (false === $current) {
            return false;
        }

        if ($current instanceof Attribute) {
            return $current;
        }

        $this->elements[$this->key()] = $current = new Attribute($current);

        return $current;
    }
}

