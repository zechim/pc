<?php

namespace Zechim\PCBundle\Service\Element;

abstract class AbstractIterator implements \Iterator, \Countable
{
    /**
     * @var array
     */
    protected $elements = [];

    /**
     * @var AbstractIterator
     */
    protected $parent;

    public function next()
    {
        return next($this->elements);
    }

    public function key()
    {
        return key($this->elements);
    }

    public function valid()
    {
        return false !== $this->current();
    }

    public function rewind()
    {
        return reset($this->elements);
    }

    public function count()
    {
        return count($this->elements);
    }

    /**
     * @return AbstractIterator
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @return bool|AbstractIterator
     */
    abstract function current();

    protected function createDefault(array $stack, array $keys, $default)
    {
        $key = array_shift($keys);

        if (true === empty($stack[$key])) {
            $stack[$key] = $default;
        }

        if (1 <= count($keys)) {
            $stack[$key] = $this->createDefault($stack[$key], $keys, $default);
        }

        return $stack;
    }
}

