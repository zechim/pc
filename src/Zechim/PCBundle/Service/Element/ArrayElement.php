<?php

namespace Zechim\PCBundle\Service\Element;

use Zechim\PCBundle\Service\Tag\Tag;

class ArrayElement extends AbstractElement
{
    public function fill(array $data)
    {
        $this->check($data);

        $data = $data['$'][$this->getName()];
        $data = $this->createDefault($data, ['@'], []);

        $tag = new Tag(
            [
                'name' => $this->getName(),
                'namespace' => $this->getNamespace(),
                'prefix' => $this->getPrefix(),
            ],
            null,
            $this->attributes->fill($data['@'])
        );

        /** @var AbstractElement $element */
        foreach ($this as $element) {
            $tag->addTag($element->fill($data));
        }

        return $tag;
    }
}