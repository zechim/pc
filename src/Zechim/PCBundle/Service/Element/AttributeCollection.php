<?php

namespace Zechim\PCBundle\Service\Element;

class AttributeCollection extends AttributeIterator
{
    public function __construct(array $attributes = [])
    {
        $this->elements = true === array_key_exists('attribute', $attributes) ? $attributes['attribute'] : [];
        $this->parent = $this;
    }

    public function fill(array $data)
    {
        /** @var Attribute $attribute */
        foreach ($this as $attribute) {
            if (true === empty($data[$attribute->getName()])) {
                continue;
            }

            $attribute->setValue($data[$attribute->getName()]);
        }

        return $this;
    }
}