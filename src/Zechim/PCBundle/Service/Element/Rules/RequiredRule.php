<?php

namespace Zechim\PCBundle\Service\Element\Rules;

use Zechim\PCBundle\Service\Element\AbstractElement;

class RequiredRule extends AbstractRule
{
    public function check(AbstractElement $element, array $data)
    {
        $required = reset($this->params);

        if (false === $this->isTrue($required)) {
            return true;
        }

        if (false === array_key_exists('$', $data)) {
            throw new \OutOfBoundsException(
                sprintf(
                    'key $ required, but "%s" was found',
                    implode(', ', array_keys($data))
                )
            );
        }

        $data = $data['$'];

        if (true === array_key_exists($element->getName(), $data)) {
            return true;
        }

        throw new \OutOfBoundsException(
            sprintf(
                'key %s required, but "%s" was found',
                $element->getName(),
                implode(', ', array_keys($data))
            )
        );
    }
}