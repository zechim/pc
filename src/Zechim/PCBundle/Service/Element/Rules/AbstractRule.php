<?php

namespace Zechim\PCBundle\Service\Element\Rules;

use Zechim\PCBundle\Service\Element\AbstractElement;

abstract class AbstractRule
{
    protected $params = [];

    public function __construct($params)
    {
        $this->params = true === is_array($params) ? $params : [$params];
    }

    protected function isTrue($value)
    {
        return true === in_array($value, ['1', 1, 'true', 't']);
    }

    abstract public function check(AbstractElement $element, array $data);
}