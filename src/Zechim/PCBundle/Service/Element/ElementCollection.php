<?php

namespace Zechim\PCBundle\Service\Element;

use Zechim\PCBundle\Service\Tag\Tag;
use Zechim\PCBundle\Service\Tag\TagCollection;

class ElementCollection extends ElementIterator
{
    public function __construct($configurationXML)
    {
        $elements = json_decode(json_encode(simplexml_load_string($configurationXML)), true);
        $elements = $this->createDefault($elements, ['element'], []);

        $this->elements = ['elements' => $elements['element']];
        $this->parent = $this;
    }

    public function fill(array $data = null)
    {
        $root = $this->current();

        $data = $this->createDefault($data, ['$', '@'], []);

        return new Tag(
            [
                'name' => $root->getName(),
                'namespace' => $root->getNamespace()
            ],
            $root->fill($data),
            $root->getAttributes()->fill($data['$']['@'])
        );
    }
}
