<?php

namespace Zechim\PCBundle\Service\Element;

class Attribute
{
    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $prefix;

    /**
     * @var string
     */
    protected $namespace;

    /**
     * @var string
     */
    protected $value;

    public function __construct(array $definition = [])
    {
        $definition = array_merge(
            [
                'name' => '',
                'prefix' => '',
                'namespace' => '',
                'value' => '',
            ],
            $definition
        );

        $this->name = (string) $definition['name'];
        $this->prefix = (string) $definition['prefix'];
        $this->namespace = (string) $definition['namespace'];
        $this->value = (string) $definition['value'];
    }

    public function getName($withPrefix = false)
    {
        if (true === $withPrefix && '' !== $this->prefix) {
            return sprintf('%s:%s', $this->prefix, $this->name);
        }

        return $this->name;
    }

    public function getNamespace()
    {
        return $this->namespace;
    }

    public function setValue($value)
    {
        $this->value = $value;
    }

    public function getValue()
    {
        return $this->value;
    }
}