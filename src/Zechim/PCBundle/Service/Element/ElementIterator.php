<?php

namespace Zechim\PCBundle\Service\Element;

class ElementIterator extends AbstractIterator
{
    /**
     * @return bool|AbstractElement
     */
    public function current()
    {
        $current = current($this->elements);

        if (false === $current) {
            return false;
        }

        $current = $this->createDefault($current, ['type'], AbstractElement::TYPE_VALUE);
        $current = $this->createDefault($current, ['namespace'], '');
        $current = $this->createDefault($current, ['prefix'], '');
        $current = $this->createDefault($current, ['rules'], []);
        $current = $this->createDefault($current, ['elements', 'element'], []);
        $current = $this->createDefault($current, ['attributes', 'attribute'], []);

        $current = $this->fixChildren($current, 'elements', 'element');
        $current = $this->fixChildren($current, 'attributes', 'attribute');

        $type = sprintf('Zechim\PCBundle\Service\Element\%sElement', ucfirst($current['type']));

        return new $type($current, $this, $this->key());
    }

    protected function fixChildren(array $stack, $key1, $key2)
    {
        if (count($stack[$key1][$key2]) >= 1 && false === array_key_exists(0, $stack[$key1][$key2])) {
            $stack[$key1][$key2] = [$stack[$key1][$key2]];
        }

        return $stack;
    }
}

