<?php

namespace Zechim\PCBundle\Service\Element;

use Zechim\PCBundle\Service\Element\Rules\AbstractRule;

abstract class AbstractElement extends ElementIterator
{
    const TYPE_VALUE = 'value';

    const TYPE_ARRAY = 'array';

    const TYPE_MULTIPLE = 'multiple';

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $prefix;

    /**
     * @var array
     */
    protected $rules;

    /**
     * @var string
     */
    protected $namespace;

    /**
     * @var AttributeCollection
     */
    protected $attributes;

    public function __construct(array $data, ElementIterator $parent = null, $key = null)
    {
        $this->elements = $data['elements']['element'];
        $this->parent = $parent;

        if (true === empty($data['name'])) {
            throw new \OutOfBoundsException(sprintf("['name'] not set on %s[%s]", $this->getPath(), $key));
        }

        $this->name = (string) $data['name'];
        $this->rules = $data['rules'];
        $this->namespace = (string) $data['namespace'];
        $this->attributes = new AttributeCollection((array) $data['attributes']);

        $this->prefix = (string) $data['prefix'];
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getPrefix()
    {
        return $this->prefix;
    }

    /**
     * @return string
     */
    public function getNamespace()
    {
        return $this->namespace;
    }

    public function getAttributes()
    {
        return $this->attributes;
    }

    public function getPath()
    {
        $path = [$this->name];
        $parent = $this->getParent();

        /** @var AbstractElement $parent */
        while (true === $parent instanceof AbstractElement) {
            $path[] = $parent->getName();

            $parent = $parent->getParent();
        }

        $path = array_reverse($path);

        return implode('.', $path);
    }

    abstract public function fill(array $data);

    public function check(array $data)
    {
        if (false === array_key_exists('$', $data)) {
            throw new \InvalidArgumentException(sprintf('"$" is not set on %s.', $this->getPath()));
        }

        if (0 === count($this->rules)) {
            return true;
        }

        foreach ($this->rules as $name => $parameter) {
            /** @var AbstractRule $rule */
            $rule = sprintf('Zechim\PCBundle\Service\Element\Rules\%sRule', ucfirst($name));

            (new $rule($parameter))->check($this, $data);
        }
    }
}