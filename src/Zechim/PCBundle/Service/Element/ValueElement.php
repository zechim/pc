<?php

namespace Zechim\PCBundle\Service\Element;

use Zechim\PCBundle\Service\Tag\Tag;

class ValueElement extends AbstractElement
{
    public function fill(array $data)
    {
        $this->check($data);

        $data = $data['$'];

        $value = null;
        $attributes = [];

        if (true === array_key_exists($this->getName(), $data)) {
            $data = $data[$this->getName()];
            $data = $this->createDefault($data, ['@'], []);

            $value = $data['$'];
            $attributes = $data['@'];
        }

        $attributes = $this->attributes->fill($attributes);

        return new Tag(
            [
                'name' => $this->getName(),
                'namespace' => $this->getNamespace(),
                'prefix' => $this->getPrefix(),
            ],
            $value,
            $attributes
        );
    }
}