<?php

namespace Zechim\PCBundle\Event;

use Zechim\PCBundle\Command\ProcessRequestCommand;
use Zechim\QueueBundle\Service\Producer\CommandProducer;

class RequestListener
{
    /**
     * @var CommandProducer
     */
    protected $producer;

    public function __construct(CommandProducer $producer)
    {
        $this->producer = $producer;
    }

    public function onNewRequestEvent(NewRequestEvent $event)
    {
        $this->producer->publish(
            ProcessRequestCommand::NAME,
            [
                ProcessRequestCommand::OPTION_REQUEST => $event->getRequest()->getId()
            ]
        );
    }
}
