<?php

namespace Zechim\PCBundle\Event;

use Symfony\Component\EventDispatcher\Event;
use Zechim\PCBundle\Entity\Request;

class NewRequestEvent extends Event
{
    const NAME = 'request.new_event';

    /**
     * @var Request
     */
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * @return Request
     */
    public function getRequest()
    {
        return $this->request;
    }
}
