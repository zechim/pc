<?php

namespace Zechim\PCBundle\Dispatcher;

use Symfony\Component\HttpFoundation\Request;
use Zechim\PCBundle\Form\ConfigurationType;
use Zechim\AppBundle\Service\Dispatcher\AbstractDispatcher;

class ConfigurationDispatcher extends AbstractDispatcher
{
    protected function createEditForm($entity, Request $request)
    {
        return $this->createForm(ConfigurationType::class, $entity);
    }

    protected function createCreateForm(Request $request)
    {
        return $this->createEditForm(null, $request);
    }
}
