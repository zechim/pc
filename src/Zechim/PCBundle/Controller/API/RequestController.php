<?php

namespace Zechim\PCBundle\Controller\API;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Zechim\PCBundle\Entity\Configuration;
use Zechim\PCBundle\Event\NewRequestEvent;
use Zechim\PCBundle\Service\Element\ElementCollection;

class RequestController extends Controller
{
    public function createAction(Request $request, Configuration $configuration)
    {
        $error = false;
        $response = [];

        try {
            $request = $this->getDoctrine()
                ->getManager()
                ->getRepository(\Zechim\PCBundle\Entity\Request::class)
                ->create($configuration, json_decode($request->getContent(), true), $this->getUser());

            $response['id'] = $request->getId();

            $this->get('event_dispatcher')->dispatch(NewRequestEvent::NAME, new NewRequestEvent($request));

        } catch (\Exception $e) {
            $error = true;
            $response['message'] = $e->getMessage();
        };

        return new JsonResponse(['error' => $error] + $response);
    }
}
