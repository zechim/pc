<?php

namespace Zechim\PCBundle\Command;

use CI\AppBundle\Entity\Server;
use CI\AppBundle\Repository\ServerRepository;
use Doctrine\Bundle\DoctrineBundle\Mapping\DisconnectedMetadataFactory;
use Doctrine\ORM\Mapping\ClassMetadata;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\HttpKernel\Bundle\BundleInterface;
use Symfony\Component\Yaml\Yaml;
use Zechim\AppBundle\Service\Dispatcher\Dumper\DispatcherDumper;
use Zechim\PCBundle\Entity\Request;
use Zechim\PCBundle\Repository\RequestRepository;
use Zechim\PCBundle\Service\Dumper\XMLDumper;
use Zechim\PCBundle\Service\Element\ElementCollection;

class ProcessRequestCommand extends ContainerAwareCommand
{
    const NAME = 'zechim:pc:process_request';
    const OPTION_REQUEST = 'request';

    protected function configure()
    {
        $this->setName(self::NAME)
            ->addOption(self::OPTION_REQUEST, self::OPTION_REQUEST, InputOption::VALUE_REQUIRED, 'Request ID');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $id = (int)$input->getOption(self::OPTION_REQUEST);

        if (0 >= $id) {
            $output->writeln('<error>request must be greater than 0</error>');
            return 1;
        }

        /** @var RequestRepository $repo */
        $repo = $this->getContainer()
            ->get('doctrine.orm.entity_manager')
            ->getRepository(Request::class);

        /** @var Request $request */
        $request = $repo->find($id);

        if (null === $request) {
            $output->writeln('<error>request not found</error>');
            return 1;
        }

        if (false === $repo->canTransform($request)) {
            $output->writeln('<error>request cannot be transformed</error>');
            return 1;
        }

        try {
            $repo->setDumping($request);

            $collection = new ElementCollection($request->getConfiguration()->getRules());
            $tags = $collection->fill($request->getInput());

            $dumper = new XMLDumper();
            $output = $dumper->dump($tags);

            $repo->setOutput($request, $output);

            return 0;

        } catch (\Exception $e) {
            $repo->setError($request, $e->getMessage());
            $output->writeln(sprintf('<error>%s</error>', $e->getMessage()));

            return 1;
        }
    }
}