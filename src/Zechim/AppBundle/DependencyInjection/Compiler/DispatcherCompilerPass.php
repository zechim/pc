<?php


namespace Zechim\AppBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Reference;
use Zechim\AppBundle\Service\Dispatcher\DispatcherParameters;

class DispatcherCompilerPass implements CompilerPassInterface
{
    /**
     * {@inheritdoc}
     */
    public function process(ContainerBuilder $container)
    {
        $routeLoader = $container->getDefinition('zechim_app.route_loader');

        foreach ($container->findTaggedServiceIds('zechim_app.dispatcher') as $id => $tags) {
            $definition = $container->getDefinition($id);
            $properties = $definition->getProperties();
            $properties['actions'] = true === array_key_exists('actions', $properties) ? $properties['actions'] : [];
            $properties['routes'] = true === array_key_exists('routes', $properties) ? $properties['routes'] : [];
            $properties['fields'] = true === array_key_exists('fields', $properties) ? $properties['fields'] : [];
            $properties['templates'] = true === array_key_exists('templates', $properties) ? $properties['templates'] : [];

            $definition->setArguments(
                [
                    $definition->getArgument(0),
                    (new Definition(DispatcherParameters::class))->setArguments(
                        [
                            $id,
                            $properties['actions'],
                            $properties['routes'],
                            $properties['fields'],
                            $properties['templates']
                        ]
                    ),
                ]
            );

            $definition->addMethodCall('setContainer', [new Reference('service_container')]);
            $definition->setProperties([]);

            $routeLoader->addMethodCall('addRouteCollectionByDispatcher', [new Reference($id)]);
        }
    }
}
