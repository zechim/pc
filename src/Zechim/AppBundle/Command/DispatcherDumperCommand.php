<?php

namespace Zechim\AppBundle\Command;

use CI\AppBundle\Entity\Server;
use CI\AppBundle\Repository\ServerRepository;
use Doctrine\Bundle\DoctrineBundle\Mapping\DisconnectedMetadataFactory;
use Doctrine\ORM\Mapping\ClassMetadata;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\HttpKernel\Bundle\BundleInterface;
use Symfony\Component\Yaml\Yaml;
use Zechim\AppBundle\Service\Dispatcher\Dumper\DispatcherDumper;

class DispatcherDumperCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('zechim:dispatcher_dump')
            ->addOption('entity', null, InputOption::VALUE_REQUIRED, 'Entity');

    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $bundle = $this->getBundle($input->getOption('entity'));
        $metadata = $this->getClassMetadata($input->getOption('entity'));

        /** @var QuestionHelper $question */
        $question = $this->getHelper('question');

        $dumper = new DispatcherDumper($bundle, $metadata);

        if ($question->ask($input, $output, new ConfirmationQuestion('Generate YML?', false))) {
            list($service, $definition) = $dumper->createServiceDefinition();
            $this->dumpYML($bundle, $service, $definition);
            $output->writeln('<info>YML generated</info>');
        }

        if ($question->ask($input, $output, new ConfirmationQuestion('Generate Migration?', false))) {
            list($name, $definition) = $dumper->createMigration();
            $this->dumpMigration($name, $definition);
            $output->writeln('<info>Migration generated</info>');
        }

        if ($question->ask($input, $output, new ConfirmationQuestion('Generate Controller?', false))) {
            list($name, $definition) = $dumper->createController();
            $this->dumpController($bundle, $name, $definition);
            $output->writeln('<info>Controller generated</info>');
        }

        if ($question->ask($input, $output, new ConfirmationQuestion('Generate Form Type?', false))) {
            list($name, $definition) = $dumper->createFormType();
            $this->dumpFormType($bundle, $name, $definition);
            $output->writeln('<info>Form Type generated</info>');
        }
    }

    protected function dumpFormType(BundleInterface $bundle, $name, $definition)
    {
        $formType = implode(DIRECTORY_SEPARATOR, [$bundle->getPath(), 'Form']);

        if (false === is_dir($formType)) {
            mkdir($formType, 0777, true);
        }

        $formType.= DIRECTORY_SEPARATOR . $name . '.php';

        file_put_contents($formType, $definition);
    }

    protected function dumpController(BundleInterface $bundle, $name, $definition)
    {
        $controller = implode(DIRECTORY_SEPARATOR, [$bundle->getPath(), 'Dispatcher']);

        if (false === is_dir($controller)) {
            mkdir($controller, 0777, true);
        }

        $controller.= DIRECTORY_SEPARATOR . $name . '.php';

        file_put_contents($controller, $definition);
    }

    protected function dumpYML(BundleInterface $bundle, $service, $definition)
    {
        $yml = implode(DIRECTORY_SEPARATOR, [$bundle->getPath(), 'Resources', 'config']);
        $yml.= DIRECTORY_SEPARATOR . 'dispatcher.yml';

        if (false === is_file($yml)) {
            file_put_contents($yml, Yaml::dump(['services' => [$service => $definition]], 5, 2));
        }

        $services = Yaml::parse(file_get_contents($yml));
        $services['services'][$service] = $definition;

        file_put_contents($yml, Yaml::dump($services, 5, 2));
    }

    protected function dumpMigration($name, $definition)
    {
        $migration = $this->getContainer()->get('kernel')->getRootDir() . DIRECTORY_SEPARATOR . 'DoctrineMigrations' . DIRECTORY_SEPARATOR . $name . '.php';

        file_put_contents($migration, $definition);
    }

    protected function convertName($string, $underline)
    {
        $string = preg_replace("/((?<=[a-z])[A-Z]|(?<!\A)[A-Z](?=[a-z]))/", $underline ? "_$1" : '$1', $string);

        return $underline ? strtolower($string) : lcfirst($string);
    }

    /**
     * @param $entity
     * @return BundleInterface
     */
    protected function getBundle($entity)
    {
        $entity = str_replace('/', '\\', $entity);

        if (false === $pos = strpos($entity, ':')) {
            throw new \InvalidArgumentException(sprintf('The entity name must contain a : ("%s") given, expecting something like ZechimAppBundle:User)', $entity));
        }

        return $this->getContainer()->get('kernel')->getBundle(substr($entity, 0, $pos));
    }

    /**
     * @param $entity
     * @return ClassMetadata
     */
    protected function getClassMetadata($entity)
    {
        return $this->getContainer()->get('doctrine.orm.entity_manager')->getClassMetadata($entity);
    }
}
