<?php

namespace Zechim\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class LoginController extends Controller
{
    public function indexAction()
    {
        return $this->redirectToRoute($this->getParameter('zechim_app.default_route'));
    }

    public function loginAction()
    {
        $helper = $this->get('security.authentication_utils');

        return $this->render('ZechimAppBundle::login.html.twig', [
            'last_username' => $helper->getLastUsername(),
            'error' => $helper->getLastAuthenticationError(),
        ]);
    }
}
