<?php

namespace Zechim\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DispatcherController extends Controller
{
    public function indexAction(Request $request)
    {
        return $this->get($request->get('_dispatcher_name'))->index($request);
    }

    public function createAction(Request $request)
    {
        return $this->get($request->get('_dispatcher_name'))->create($request);
    }

    public function editAction(Request $request)
    {
        return $this->get($request->get('_dispatcher_name'))->edit($request);
    }

    public function showAction(Request $request)
    {
        return $this->get($request->get('_dispatcher_name'))->show($request);
    }

    public function deleteAction(Request $request)
    {
        return $this->get($request->get('_dispatcher_name'))->delete($request);
    }
}
