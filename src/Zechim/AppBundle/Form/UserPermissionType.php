<?php

namespace Zechim\AppBundle\Form;

use Zechim\AppBundle\Entity\UserAction;
use Zechim\AppBundle\Entity\UserPermission;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserPermissionType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', null, [
            'label' => 'trans.label.name',
            'attr' => ['class' => 'form-control'],
        ]);

        $builder->add('userAction', EntityType::class, [
            'label' => 'trans.label.user_action',
            'expanded' => false,
            'multiple' => true,
            'empty_data' => null,
            'class' => UserAction::class,
            'attr' => ['class' => 'form-control']
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => UserPermission::class,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'zechim_app_user_permission';
    }
}
