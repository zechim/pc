<?php

namespace Zechim\AppBundle\Form;

use Zechim\AppBundle\Entity\User;
use Zechim\AppBundle\Entity\UserPermission;
use Zechim\AppBundle\Form\Subscriber\UserPasswordSubscriber;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;

class UserType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('username', null, [
            'label' => 'trans.label.username',
            'attr' => [
                'class' => 'form-control',
            ],
            'constraints' => [new NotBlank()]
        ]);

        $builder->add('name', null, [
            'label' => 'trans.label.name',
            'attr' => [
                'class' => 'form-control',
            ],
            'constraints' => [new NotBlank()]
        ]);

        $builder->add('userPermission', EntityType::class, [
            'label' => 'trans.label.user_permission',
            'expanded' => false,
            'multiple' => true,
            'by_reference' => true,
            'class' => UserPermission::class,
            'attr' => [
                'class' => 'form-control',
            ],
            'constraints' => [new NotNull()]
        ]);

        $builder->add('active', ChoiceType::class, [
            'label' => 'trans.label.active',
            'expanded' => true,
            'choices' => [
                'trans.message.yes' => 1,
                'trans.message.no' => 0
            ],
            'attr' => [
                'class' => 'form-control',
            ],
            'constraints' => [new NotBlank()]
        ]);

        # fix google chrome autocomplete
        $builder->add('pass', PasswordType::class, [
            'required' => false,
            'label' => false,
            'mapped' => false,
            'attr' => [
                'autocomplete' => 'off',
                'class' => 'hidden'
            ]
        ]);

        $builder->add('password', RepeatedType::class, [
            'type' => PasswordType::class,
            'required' => false,
            'first_options'  => [
                'label' => 'trans.label.password',
                'attr' => [
                    'autocomplete' => 'off',
                    'class' => 'form-control'
                ],
            ],
            'second_options' => [
                'label' => 'trans.label.repeat.password',
                'attr' => [
                    'autocomplete' => 'off',
                    'class' => 'form-control'
                ],
            ],
        ]);

        $builder->addEventSubscriber(new UserPasswordSubscriber($options['encoder'], $options['translator']));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => User::class,
            'constraints' => [new UniqueEntity(['fields' => 'username'])]
        ));

        $resolver->setRequired(['encoder', 'translator']);

        $resolver->setAllowedTypes('encoder', [EncoderFactoryInterface::class]);
        $resolver->setAllowedTypes('translator', [TranslatorInterface::class]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'zechim_app_user';
    }


}
