<?php

namespace Zechim\AppBundle\Form\Subscriber;

use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\FormError;
use Zechim\AppBundle\Entity\User;
use Symfony\Component\Translation\TranslatorInterface;

class UserPasswordSubscriber implements EventSubscriberInterface
{
    /**
     * @var \Symfony\Component\Security\Core\Encoder\EncoderFactory
     */
    protected $passwordEncoder;

    /**
     * @var \Symfony\Component\Translation\TranslatorInterface
     */
    protected $translator;

    /**
     * @var string
     */
    protected $password;

    /**
     * UserPasswordSubscriber constructor.
     * @param EncoderFactoryInterface $passwordEncoder
     * @param TranslatorInterface $translator
     */
    public function __construct(EncoderFactoryInterface $passwordEncoder, TranslatorInterface $translator)
    {
        $this->passwordEncoder = $passwordEncoder;
        $this->translator = $translator;
    }
    
    /**
     * @param User $user
     * @return mixed|\Symfony\Component\Security\Core\Encoder\PasswordEncoderInterface
     */
    private function getEncoder(User $user)
    {
        return $this->passwordEncoder->getEncoder($user);
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [FormEvents::PRE_SET_DATA => 'preSetData', FormEvents::POST_SUBMIT => 'postSubmit'];
    }

    /**
     * @param FormEvent $event
     */
    public function preSetData(FormEvent $event)
    {
        $this->password = true === $event->getData() instanceof User ? $event->getData()->getPassword() : null;
    }

    /**
     * @param FormEvent $event
     */
    public function postSubmit(FormEvent $event)
    {
        $user = $event->getData();

        if (null === $this->password && null === $user->getPassword()) {
            $event->getForm()->get('password')->get('first')->addError(new FormError($this->translator->trans('trans.message.invalid.password')));
        }
        
        if (null === $this->password  || (null !== $this->password && null !== $user->getPassword())) {
            $user->setPassword($this->getEncoder($user)->encodePassword($user->getPassword(), $user->getSalt()));
            
        } else {
            $user->setPassword($this->password);
        }
    }
}
