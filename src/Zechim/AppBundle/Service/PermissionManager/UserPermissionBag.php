<?php

namespace Zechim\AppBundle\Service\PermissionManager;

use Zechim\AppBundle\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Zechim\AppBundle\Entity\UserAction;

class UserPermissionBag
{
    /**
     * @var \Zechim\AppBundle\Entity\User
     */
    protected $user;
    
    /**
     * @var ArrayCollection
     */
    protected $actionList;
    
    /**
     * @var \Doctrine\ORM\EntityManagerInterface
     */
    protected $em;

    public function __construct(User $user, EntityManagerInterface $em)
    {
        $this->user = $user;
        $this->em = $em;
    }
    
    /**
     * @return ArrayCollection
     */
    protected function getActionList()
    {
        if (null !== $this->actionList) {
            return $this->actionList;
        }
        
        return $this->actionList = $this->em->getRepository('ZechimAppBundle:UserAction')->getActionByUser($this->user);
    }

    /**
     * @param $code
     * @return bool
     */
    public function hasAction($code)
    {
        return $this->getActionList()->filter(function (UserAction $ua) use ($code) {
            return $code === $ua->getCode();
        })->count() >= 1;
    }
}