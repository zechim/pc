<?php

namespace Zechim\AppBundle\Service\Dispatcher;

class ActionCollection implements \IteratorAggregate, \Countable
{
    protected $actions;

    public function __construct(RouteCollection $routes, array $actions)
    {
        $actions = 0 === count($actions) ? $this->getDefaultActions() : $actions;

        foreach ($actions as $parameters) {
            $this->actions[$parameters['action']] = new DispatcherAction($parameters, $routes->getByAction($parameters['action']));
        }
    }

    public function getIterator()
    {
        return new \ArrayIterator($this->actions);
    }

    public function getByToolbar($toolbar)
    {
        return array_filter($this->actions, function (DispatcherAction $action) use ($toolbar) {
            return true === in_array($toolbar, $action->getToolbars());
        });
    }

    public function getByAction($action)
    {
        return $this->actions[$action];
    }

    public function count()
    {
        return count($this->actions);
    }

    protected function getDefaultActions()
    {
        return [
            ['label' => 'index', 'class' => null, 'icon' => null, 'action' => 'index', 'toolbars' => ['ungrouped']],
            ['label' => 'create', 'class' => 'info', 'icon' => 'plus', 'action' => 'create', 'toolbars' => ['toolbar']],
            ['label' => 'show', 'class' => 'info', 'icon' => 'search', 'action' => 'show', 'toolbars' => ['datagrid']],
            ['label' => 'edit', 'class' => 'info', 'icon' => 'edit', 'action' => 'edit', 'toolbars' => ['datagrid', 'show']],
            ['label' => 'delete', 'class' => 'danger', 'icon' => 'remove', 'action' => 'delete', 'toolbars' => ['datagrid', 'show']],
        ];
    }
}