<?php

namespace Zechim\AppBundle\Service\Dispatcher;

interface PropertyReaderInterface
{
    /**
     * @param $object
     * @param array $parameters
     * @param $path array or string
     * @return mixed
     */
    public function getValue($object, array $parameters = [], $path);
}