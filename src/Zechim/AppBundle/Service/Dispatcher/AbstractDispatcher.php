<?php

namespace Zechim\AppBundle\Service\Dispatcher;

use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\ORM\EntityRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouteCollection;
use Zechim\AppBundle\Service\Pagination\Factory;
use Zechim\AppBundle\Service\Pagination\Pagination;
use Zechim\AppBundle\Twig\MessageExtension;

abstract class AbstractDispatcher extends Controller
{
    protected $className;

    /**
     * @var DispatcherParameters
     */
    protected $dispatcherParameters;

    public function __construct($className, DispatcherParameters $dispatcherParameters)
    {
        $this->className = $className;
        $this->dispatcherParameters = $dispatcherParameters;
    }

    public function createRouteCollection()
    {
        $routeCollection = new RouteCollection();

        /** @var \Zechim\AppBundle\Service\Dispatcher\Route $route */
        foreach ($this->dispatcherParameters->getRoutes() as $route) {
            $routeCollection->add($route->getName(), $route->createRoute());
        }

        return $routeCollection;
    }

    /**
     * @param $name
     * @return string
     */
    protected function getTemplate($name)
    {
        return $this->dispatcherParameters->getTemplate($name);
    }

    /**
     * @param $name
     * @return DispatcherRoute
     */
    public function getRouteByAction($action)
    {
        return $this->dispatcherParameters->getRouteByAction($action);
    }

    protected function getMessage($id)
    {
        return $this->get('translator')->trans($this->dispatcherParameters->getMessage($id));
    }

    /**
     * @return ObjectRepository|EntityRepository
     */
    protected function getRepository()
    {
        return $this->getDoctrine()->getManager()->getRepository($this->className);
    }

    /**
     * @param Request $request
     * @return Pagination
     */
    protected function createPagination(Request $request)
    {
        return Factory::getPagination(
            $this->getRepository()->createQueryBuilder('o'),
            $request->get('page'),
            $request->get('limit')
        );
    }

    /**
     * @param $entity
     * @param Request $request
     * @return array
     */
    abstract protected function createEditForm($entity, Request $request);

    /**
     * @param $entity
     * @param Request $request
     * @return array
     */
    abstract protected function createCreateForm(Request $request);

    /**
     * @param $entity
     * @param Request $request
     * @return array
     */
    protected function createShowParameters($entity, Request $request)
    {
        return [];
    }

    /**
     * @param FormInterface $form
     * @param Request $request
     * @return mixed
     */
    protected function doEdit(FormInterface $form, Request $request)
    {
        $this->getDoctrine()->getManager()->persist($form->getData());
        $this->getDoctrine()->getManager()->flush();

        $repo = $this->getRepository();
        $data = $form->getData();

        if (true === method_exists($repo, 'edit')) {
            $repo->edit($data, $this->getUser());

            return $data;
        }

        $this->getDoctrine()->getManager()->persist($data);
        $this->getDoctrine()->getManager()->flush();

        return $data;
    }

    /**
     * @param FormInterface $form
     * @param Request $request
     * @return mixed
     */
    protected function doCreate(FormInterface $form, Request $request)
    {
        $repo = $this->getRepository();
        $data = $form->getData();

        if (true === method_exists($repo, 'create')) {
            $repo->create($data, $this->getUser());

            return $data;
        }

        $this->getDoctrine()->getManager()->persist($data);
        $this->getDoctrine()->getManager()->flush();

        return $data;
    }

    /**
     * @param $entity
     */
    protected function doDelete($entity)
    {
        $this->getDoctrine()->getManager()->remove($entity);
        $this->getDoctrine()->getManager()->flush();
    }

    /**
     * @param Request $request
     * @return null|object
     */
    protected function getEntity(Request $request)
    {
        $entity = $this->getRepository()->findOneBy(['id' => $id = $request->get('id')]);

        if (null === $entity) {
            throw new \InvalidArgumentException(sprintf('entity %s not found', $id));
        }

        return $entity;
    }

    protected function setFlashMessage($type, $message)
    {
        $this->get('session')->getFlashBag()->add($type, $message);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(Request $request)
    {
        $pagination = $this->createPagination($request);
        $pagination->set(Pagination::ROUTE, $this->getRouteByAction('index')->getName());

        return $this->render(
            $this->getTemplate('index'),
            [
                'pagination' => $pagination,
                'fields' => $this->dispatcherParameters->getFieldsByGroup('index'),
                'actions' => $this->dispatcherParameters->getActions(),
                'templates' => $this->dispatcherParameters->getTemplates(),
                'router' => $this->get('router'),
                'request' => $request
            ]
        );
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function show(Request $request)
    {
        $entity = $this->getEntity($request);

        return $this->render(
            $this->getTemplate('show'),
            [
                'entity' => $entity,
                'fields' => $this->dispatcherParameters->getFieldsByGroup('show'),
                'actions' => $this->dispatcherParameters->getActions(),
                'router' => $this->get('router'),
                'request' => $request,
                'parameters' => $this->createShowParameters($entity, $request),
            ]
        );
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(Request $request)
    {
        $entity = $this->getEntity($request);

        $form = $this->createEditForm($entity, $request);
        $form->handleRequest($request);

        if ('POST' === $request->getMethod() && true === $form->isValid()) {
            $this->doEdit($form, $request);

            $this->setFlashMessage(MessageExtension::SUCCESS, $this->getMessage('edited'));

            return $this->redirect($this->getRouteByAction('show')->generate($this->get('router'), $entity, ['request' => $request]));
        }

        return $this->render(
            $this->getTemplate('edit'),
            [
                'entity' => $entity,
                'form' => $form->createView(),
                'actions' => $this->dispatcherParameters->getActions(),
                'router' => $this->get('router'),
                'request' => $request
            ]
        );
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(Request $request)
    {
        $form = $this->createCreateForm($request);
        $form->handleRequest($request);

        if ('POST' === $request->getMethod() && true === $form->isValid()) {
            $entity = $this->doCreate($form, $request);

            $this->setFlashMessage(MessageExtension::SUCCESS, $this->getMessage('created'));

            return $this->redirect($this->getRouteByAction('show')->generate($this->get('router'), $entity, ['request' => $request]));
        }

        return $this->render(
            $this->getTemplate('create'),
            [
                'form' => $form->createView(),
                'actions' => $this->dispatcherParameters->getActions(),
                'router' => $this->get('router'),
                'request' => $request
            ]
        );
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function delete(Request $request)
    {
        $entity = $this->getEntity($request);

        try {
            $this->doDelete($entity);

            $this->setFlashMessage(MessageExtension::SUCCESS, $this->getMessage('deleted'));

        } catch (\Exception $e) {
            $this->setFlashMessage(MessageExtension::ERROR, $this->get('translator')->trans($e->getMessage()));
        }

        return $this->redirect($this->getRouteByAction('index')->generate($this->get('router'), $entity, ['request' => $request]));
    }

}