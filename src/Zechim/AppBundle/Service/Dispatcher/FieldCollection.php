<?php

namespace Zechim\AppBundle\Service\Dispatcher;

class FieldCollection implements \IteratorAggregate, \Countable
{
    protected $fields = [];

    public function __construct(array $fields = [])
    {
        foreach ($fields as $parameters) {
            foreach ($parameters['fields'] as $field) {
                $field = new Field(new PropertyReader(), $field['path'], $field['label']);

                $this->fields[$parameters['group']] = true === array_key_exists($parameters['group'], $this->fields) ? $this->fields[$parameters['group']] : [];
                $this->fields[$parameters['group']][] = $field;
            }
        }
    }

    public function getIterator()
    {
        return new \ArrayIterator($this->fields);
    }

    public function getByGroup($groupName)
    {
        if (false === array_key_exists($groupName, $this->fields)) {
            return new \ArrayIterator([]);
        }

        return new \ArrayIterator($this->fields[$groupName]);
    }

    public function count()
    {
        return count($this->fields);
    }
}