<?php

namespace Zechim\AppBundle\Service\Dispatcher;

class RouteCollection implements \IteratorAggregate, \Countable
{
    protected $routes = [];

    public function __construct(PropertyReaderInterface $reader, $dispatcherName, array $routes = [])
    {
        foreach ($routes as $parameters) {
            $this->routes[$parameters['action']] = new DispatcherRoute($reader, $dispatcherName, $parameters);
        }
    }

    public function getIterator()
    {
        return new \ArrayIterator($this->routes);
    }

    /**
     * @param $actionName
     * @return DispatcherRoute
     */
    public function getByAction($actionName)
    {
        return $this->routes[$actionName];
    }

    public function count()
    {
        return count($this->routes);
    }
}