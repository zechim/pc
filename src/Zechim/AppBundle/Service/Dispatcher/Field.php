<?php

namespace Zechim\AppBundle\Service\Dispatcher;

class Field
{
    /**
     * @var PropertyReaderInterface
     */
    protected $reader;

    protected $path;

    protected $label;

    public function __construct(PropertyReaderInterface $reader, $path, $label)
    {
        $this->reader = $reader;
        $this->path = $path;
        $this->label = $label;
    }

    public function getValue($object, array $parameters = [])
    {
        return $this->reader->getValue($object, $parameters, $this->path);
    }

    public function getLabel()
    {
        return $this->label;
    }
}