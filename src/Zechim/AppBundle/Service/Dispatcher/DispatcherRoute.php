<?php

namespace Zechim\AppBundle\Service\Dispatcher;

use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\Router;
use Zechim\AppBundle\Controller\DispatcherController;

class DispatcherRoute
{
    /**
     * @var PropertyReaderInterface
     */
    protected $reader;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $path;

    protected $parameters = [];

    protected $roles = [];

    protected $defaults = [];

    protected $requirements = [];

    public function __construct(PropertyReaderInterface $reader, $dispatcherName, array $properties)
    {
        $this->reader = $reader;

        $this->name = $properties['name'];
        $this->path = $properties['path'];
        $this->parameters = true === array_key_exists('parameters', $properties) ? $properties['parameters'] : [];
        $this->roles = true === array_key_exists('roles', $properties) ? $properties['roles'] : [];

        $defaults = true === array_key_exists('defaults', $properties) ? $properties['defaults'] : [];
        $defaults['_dispatcher_name'] = $dispatcherName;
        $defaults['_controller'] = sprintf('%s::%sAction', DispatcherController::class, $properties['action']);

        $this->defaults = $defaults;
        $this->requirements = true === array_key_exists('requirements', $properties) ? $properties['requirements'] : [];;
    }

    public function generate(Router $router, $object, array $parameters = [])
    {
        return $router->generate($this->name, $this->reader->getValue($object, $parameters, $this->parameters));
    }

    /**
     * @return mixed|string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return Route
     */
    public function createRoute()
    {
        return new Route($this->path, $this->defaults, $this->requirements);
    }

    public function getRoles()
    {
        return $this->roles;
    }
}