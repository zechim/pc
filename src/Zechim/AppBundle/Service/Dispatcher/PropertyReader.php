<?php

namespace Zechim\AppBundle\Service\Dispatcher;

use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\PropertyAccess\PropertyAccessor;

class PropertyReader implements PropertyReaderInterface
{
    /**
     * @var PropertyAccessor
     */
    protected $propertyAccessor;

    /**
     * @return PropertyAccessor
     */
    protected function getPropertyAccessor()
    {
        if (null === $this->propertyAccessor) {
            $this->propertyAccessor = PropertyAccess::createPropertyAccessor();
        }

        return $this->propertyAccessor;
    }

    /**
     * @param $object
     * @param array $parameters
     * @param $path
     * @return mixed
     */
    public function getValue($object, array $parameters = [], $path)
    {
        $object = ['entity' => $object] + $parameters;

        if (false === is_array($path)) {
            return $this->getPropertyAccessor()->getValue($object, $path);
        }

        foreach ($path as $name => $property) {
            $path[$name] = $this->getPropertyAccessor()->getValue($object, $property);
        }

        return $path;

    }
}