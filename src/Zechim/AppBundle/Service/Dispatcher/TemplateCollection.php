<?php

namespace Zechim\AppBundle\Service\Dispatcher;

class TemplateCollection extends \ArrayObject
{
    protected $templates = [
        'index' => 'ZechimAppBundle:Dispatcher:index.html.twig',
        'show' => 'ZechimAppBundle:Dispatcher:show.html.twig',
        'edit' => 'ZechimAppBundle:Dispatcher:edit.html.twig',
        'create' => 'ZechimAppBundle:Dispatcher:create.html.twig',
    ];

    public function __construct(array $templates)
    {
        parent::__construct(array_merge($this->templates, $templates));
    }
}