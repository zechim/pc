<?php

namespace Zechim\AppBundle\Service\Dispatcher\Dumper;


use Doctrine\ORM\Mapping\ClassMetadata;
use Symfony\Component\HttpKernel\Bundle\BundleInterface;

class DispatcherDumper
{
    /**
     * @var ClassMetadata
     */
    protected $metadata;

    /**
     * @var BundleInterface
     */
    protected $bundle;

    public function __construct(BundleInterface $bundle, ClassMetadata $metadata)
    {
        $this->bundle = $bundle;
        $this->metadata = $metadata;
    }

    public function createServiceDefinition()
    {
        $entity = $this->getEntity();
        $underline = $this->camelToUnderline($entity);
        $prefix = $this->getPrefix();
        $path = str_replace('_', '', $prefix);

        $role = $prefix . '_' . $underline;

        $key = sprintf('zechim.dispatcher.%s', $underline);

        $data['class'] = sprintf('%s\Dispatcher\%sDispatcher', $this->bundle->getNamespace(), $entity);
        $data['arguments'] = [$this->metadata->getName()];
        $data['tags'] = [['name' => 'zechim_app.dispatcher']];
        $data['properties'] = [];

        $data['properties']['routes'][] = [
            'action'        => 'index',
            'roles'         => [sprintf('ROLE_%s_INDEX', strtoupper($role))],
            'name'          => sprintf('%s_%s_index', $prefix, $underline),
            'path'          => sprintf('%s/%s/index', $path, $underline),
            'defaults'      => ['page' => 1, 'limit' => 10],
            'requirements'  => ['page']
        ];

        $data['properties']['routes'][] = [
            'action'        => 'create',
            'roles'         => [sprintf('ROLE_%s_CREATE', strtoupper($role))],
            'name'          => sprintf('%s_%s_create', $prefix, $underline),
            'path'          => sprintf('%s/%s/create', $path, $underline),
        ];

        $data['properties']['routes'][] = [
            'action'        => 'show',
            'roles'         => [sprintf('ROLE_%s_SHOW', strtoupper($role))],
            'name'          => sprintf('%s_%s_show', $prefix, $underline),
            'path'          => sprintf('%s/%s/show/{id}', $path, $underline),
            'parameters'    => ['id' => '[entity].id'],
            'requirements'  => ['id']
        ];

        $data['properties']['routes'][] = [
            'action'        => 'edit',
            'roles'         => [sprintf('ROLE_%s_EDIT', strtoupper($role))],
            'name'          => sprintf('%s_%s_edit', $prefix, $underline),
            'path'          => sprintf('%s/%s/edit/{id}', $path, $underline),
            'parameters'    => ['id' => '[entity].id'],
            'requirements'  => ['id']
        ];

        $data['properties']['routes'][] = [
            'action'        => 'delete',
            'roles'         => [sprintf('ROLE_%s_DELETE', strtoupper($role))],
            'name'          => sprintf('%s_%s_delete', $prefix, $underline),
            'path'          => sprintf('%s/%s/delete/{id}', $path, $underline),
            'parameters'    => ['id' => '[entity].id'],
            'requirements'  => ['id']
        ];

//        $data['properties']['actions'] = [
//            ['label' => 'index', 'class' => null, 'icon' => null, 'action' => 'index', 'toolbars' => ['ungrouped']],
//            ['label' => 'create', 'class' => 'info', 'icon' => 'plus', 'action' => 'create', 'toolbars' => ['toolbar']],
//            ['label' => 'show', 'class' => 'info', 'icon' => 'search', 'action' => 'show', 'toolbars' => ['datagrid']],
//            ['label' => 'edit', 'class' => 'info', 'icon' => 'edit', 'action' => 'edit', 'toolbars' => ['datagrid', 'show']],
//            ['label' => 'delete', 'class' => 'danger', 'icon' => 'remove', 'action' => 'delete', 'toolbars' => ['datagrid', 'show']],
//        ];

        $fields = [];

        foreach ($this->getFields() as $field) {
            $fields[] = ['path' => sprintf('[entity].%s', $field['fieldName']), 'label' => sprintf('trans.label.%s', $field['fieldName'])];
        }

        $data['properties']['fields'][] = ['group' => 'index', 'fields' => $fields];
        $data['properties']['fields'][] = ['group' => 'show', 'fields' => $fields];

        return [$key, $data];
    }

    public function createMigration()
    {
        $version = 'Version' . date('YmdHms');
        $entity = $this->camelToUnderline($this->getEntity());
        $prefix = $this->getPrefix();

        $migration = <<<MIGRATION
<?php

namespace Application\Migrations;

use Zechim\AppBundle\Doctrine\DBAL\Migrations\AbstractAppMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class $version extends AbstractAppMigration
{
    protected \$prefix = '$prefix';
    
    /**
     * @param Schema \$schema
     */
    public function up(Schema \$schema)
    {
        \$icon = '';
        \$parent = null;
        
        \$this->createRoute(\$this->prefix, '$entity', 'index', \$icon, \$parent, true);
        \$this->createRoute(\$this->prefix, '$entity', 'show');
        \$this->createRoute(\$this->prefix, '$entity', 'create');
        \$this->createRoute(\$this->prefix, '$entity', 'edit');
        \$this->createRoute(\$this->prefix, '$entity', 'delete');

        \$this->createRouteUp();
    }

    /**
     * @param Schema \$schema
     */ 
    public function down(Schema \$schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        \$this->deleteRoute(\$this->prefix, '$entity', 'delete');
        \$this->deleteRoute(\$this->prefix, '$entity', 'edit');
        \$this->deleteRoute(\$this->prefix, '$entity', 'create');
        \$this->deleteRoute(\$this->prefix, '$entity', 'show');
        \$this->deleteRoute(\$this->prefix, '$entity', 'index');
    }
}

MIGRATION;

        return [$version, $migration];
    }

    public function createController()
    {
        $name = $this->getEntity();
        $namespace = $this->bundle->getNamespace();

        $controller = <<<CONTROLLER
<?php

namespace $namespace\Dispatcher;

use Symfony\Component\HttpFoundation\Request;
use {$namespace}\Form\\{$name}Type;
use Zechim\AppBundle\Service\Dispatcher\AbstractDispatcher;

class {$name}Dispatcher extends AbstractDispatcher
{
    protected function createEditForm(\$entity, Request \$request)
    {
        return \$this->createForm({$name}Type::class, \$entity);
    }

    protected function createCreateForm(Request \$request)
    {
        return \$this->createEditForm(null, \$request);
    }
}

CONTROLLER;

        return [sprintf('%sDispatcher', $name), $controller];
    }

    public function createFormType()
    {
        $name = $this->getEntity();
        $namespace = $this->bundle->getNamespace();
        $entity = $this->metadata->getName();
        $underline = $this->camelToUnderline($name);
        $fields = $this->getFields();
        $prefix = $this->getPrefix();

        $notNull = array_filter($fields, function ($field) {
            return true === array_key_exists('nullable', $field) && true === $field['nullable'];
        });

        $formType = <<<FORM
<?php

namespace $namespace\Form;

use {$entity};
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
FORM;

    if (true === 1 <= count($notNull)) {
        $formType.= <<<FORM

use Symfony\Component\Validator\Constraints\NotNull;

FORM;

    }

$formType.= <<<FORM

class {$name}Type extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface \$builder, array \$options)
    {
FORM;

        foreach ($fields as $field) {
            $fieldName = $field['fieldName'];

            $formType.= <<<FORM

        \$builder->add('{$fieldName}', null, [
            'label' => 'trans.label.{$fieldName}',
            'attr' => [
                'class' => 'form-control',
            ],
FORM;

            if (true === array_key_exists('nullable', $field) && true === $field['nullable']) {
                $formType.= <<<FORM

            'constraints' => [new NotNull()]
FORM;
            }

            $formType.= <<<FORM

        ]);
        
FORM;

        }

        $formType.= <<<FORM

    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver \$resolver)
    {
        \$resolver->setDefaults(array(
            'data_class' => {$name}::class,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return '{$prefix}_{$underline}';
    }
}

FORM;

        return [sprintf('%sType', $name), $formType];
    }

    protected function getFields()
    {
        $fields = [];

        foreach ($this->metadata->fieldMappings as $field) {
            if (true === array_key_exists('id', $field)) {
                continue;
            }

            if (true === in_array($field['type'], ['datetime'])) {
                continue;
            }

            $field['label'] = sprintf('trans.label.%s', $field['fieldName']);

            $fields[] = $field;
        }

        return $fields;
    }

    protected function getEntity()
    {
        $entity = explode('\\', $this->metadata->getName());
        return end($entity);
    }

    protected function camelToUnderline($string)
    {
        return strtolower(preg_replace("/((?<=[a-z])[A-Z]|(?<!\A)[A-Z](?=[a-z]))/", "_$1", $string));
    }

    protected function getPrefix()
    {
        $prefix = $this->camelToUnderline($this->bundle->getName(), true);

        return str_replace('_bundle', '', $prefix);
    }
}