<?php

namespace Zechim\AppBundle\Service\Dispatcher;

class DispatcherParameters
{
    protected $actions = [];

    protected $messages = [
        'created' => 'trans.message.created',
        'deleted' => 'trans.message.deleted',
        'edited' => 'trans.message.edited',
    ];

    /**
     * @var FieldCollection
     */
    protected $fields;

    public function __construct($dispatcherName, array $actions = [], array $routes = [], array $fields = [], array $templates = [])
    {
        $this->fields = new FieldCollection($fields);
        $this->routes = new RouteCollection(new PropertyReader(), $dispatcherName, $routes);
        $this->actions = new ActionCollection($this->routes, $actions);
        $this->templates = new TemplateCollection($templates);
    }

    /**
     * @param $action
     * @return DispatcherRoute
     */
    public function getRouteByAction($action)
    {
        return $this->routes->getByAction($action);
    }

    public function getTemplate($name)
    {
        return $this->templates[$name];
    }

    public function getMessage($id)
    {
        return $this->messages[$id];
    }

    public function getFieldsByGroup($groupName)
    {
        return $this->fields->getByGroup($groupName);
    }

    public function getActions()
    {
        return $this->actions;
    }

    public function getTemplates()
    {
        return $this->templates;
    }

    public function getRoutes()
    {
        return $this->routes;
    }
}