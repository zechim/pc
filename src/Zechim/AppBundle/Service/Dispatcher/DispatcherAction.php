<?php

namespace Zechim\AppBundle\Service\Dispatcher;

class DispatcherAction
{
    /**
     * @var DispatcherRoute
     */
    protected $route;

    protected $icon;

    protected $class;

    protected $toolbars = [];

    public function __construct(array $properties, DispatcherRoute $dispatcherRoute)
    {
        $this->label = $properties['label'];
        $this->class = $properties['class'];
        $this->icon = $properties['icon'];
        $this->toolbars = $properties['toolbars'];
        $this->dispatcherRoute = $dispatcherRoute;
    }

    public function getIcon()
    {
        return $this->icon;
    }

    public function getClass()
    {
        return $this->class;
    }

    public function getToolbars()
    {
        return $this->toolbars;
    }

    /**
     * @return DispatcherRoute
     */
    public function getDispatcherRoute()
    {
        return $this->dispatcherRoute;
    }
}