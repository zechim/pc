<?php

namespace Zechim\AppBundle\Service\Menu;

use Zechim\AppBundle\Entity\Menu;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Translation\TranslatorInterface;

class MenuRequestFetcher
{
    /**
     * @var \Zechim\AppBundle\Service\PermissionManager\PermissionManager
     */
    protected $pm;
    
    /**
     * @var \Doctrine\ORM\EntityManagerInterface
     */
    protected $em;
    
    /**
     * @var \Symfony\Bundle\FrameworkBundle\Translation\Translator
     */
    protected $translator;
    
    /** 
     * @var boolean
     */
    protected $isSkipped = false;
    
    /**
     * @var \Zechim\AppBundle\Entity\Menu
     */
    protected $menu;
    
    /**
     * @var string
     */
    protected $route;
    
    /**
     * @var string
     */
    protected $pageName;
    
    /**
     * MenuRequestFetcher constructor.
     * @param EntityManager $em
     * @param TranslatorInterface $translator
     */
    public function __construct(EntityManager $em, TranslatorInterface $translator)
    {
        $this->em = $em;
        $this->translator = $translator;
    }
    
    /**
     * @param Request $request
     */
    public function fetch(Request $request)
    {
        $route = $request->attributes->get('_route');
        
        if (null === $route) {
            $this->isSkipped = true;
            return;
        }

        foreach (['_assetic', '_profiler', '_wdt', '_configurator', '_error', '_app_login', '_app_index', 'zechim_app_default_route'] as $check) {
            if (false !== strpos($route, $check)) {
                $this->isSkipped = true;
                return;
            }
        }

        $this->menu = $this->em->getRepository('ZechimAppBundle:Menu')->findOneBy(['route' => $route]);
        $this->route = $route;
        
        if ($this->menu instanceof Menu) {
            if (null === $request->getSession()->get('last_route')) {
                $request->getSession()->set('last_route', [$route, $request->get('_route_params', [])]);
            }

            $this->pageName = $this->translator->trans($this->menu->getPageName());
        }
    }
    
    /**
     * @return boolean
     */
    public function getIsSkipped()
    {
        return $this->isSkipped;
    }
   
    /**
     * @return \Zechim\AppBundle\Entity\Menu
     */
    public function getMenu()
    {
        return $this->menu;
    }
    
    /**
     * @return string
     */
    public function getRoute()
    {
        return $this->route;
    }
    
    /**
     * @return string
     */
    public function getPageName()
    {
        return $this->pageName;
    }
}