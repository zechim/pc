<?php

namespace Zechim\AppBundle\Service\Menu;

use Zechim\AppBundle\Entity\Menu;
use Zechim\AppBundle\Service\PermissionManager\PermissionManager;
use Zechim\AppBundle\Entity\UserAction;

class MenuItemIterator extends AbstractMenuIterator
{
    /**
     * @var \Zechim\AppBundle\Entity\Menu
     */
    protected $item;
    
    /**
     * @var \ArrayObject
     */
    protected $raw;
    
    /** 
     * @var array
     */
    protected $data;

    /**
     * @var \Zechim\AppBundle\Service\PermissionManager\PermissionManager
     */
    protected $pm;
    
    /**
     * @param \ArrayObject $data
     * @param Menu $item
     * @param PermissionManager $pm
     */
    public function __construct(\ArrayObject $data, Menu $item, PermissionManager $pm)
    {
        $this->item = $item;
        $this->raw = $data;
        $this->pm = $pm;

        $this->data = array_filter($this->raw->getArrayCopy(), function(Menu $menu) use ($pm) {
            $isChild = $menu->getParent() && $menu->getParent()->getId() == $this->item->getId();
            
            if (false === $isChild) {
                return false;
            }
            
            return $this->checkPermission($menu, $pm);
        });
        
        uasort($this->data, array($this, 'usort'));
    }

    /**
     * @return \Zechim\AppBundle\Entity\Menu
     */
    public function getItem()
    {
        return $this->item;
    }

    public function current()
    {
        $current = current($this->data);

        if (false === $current || null === $current) {
            return false;
        }
        
        return new MenuItemIterator($this->raw, $current, $this->pm);
    }
}