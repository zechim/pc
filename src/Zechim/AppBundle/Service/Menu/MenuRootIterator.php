<?php

namespace Zechim\AppBundle\Service\Menu;

use Zechim\AppBundle\Entity\Menu;
use Zechim\AppBundle\Service\PermissionManager\PermissionManager;

class MenuRootIterator extends AbstractMenuIterator
{
    /**
     * @var array
     */
    protected $data;
    
    /**
     * @var \ArrayObject
     */
    protected $raw;
    
    /**
     * @var \Zechim\AppBundle\Service\PermissionManager\PermissionManager
     */
    protected $pm;
    
    public function __construct(\ArrayObject $data, PermissionManager $pm)
    {
        $this->raw = $data;
        $this->pm = $pm;
        
        $this->data = array_filter($this->raw->getArrayCopy(), function(Menu $menu) use ($pm) {
            if (null !== $menu->getParent()) {
                return false;
            }
           
            return $this->checkPermission($menu, $pm);
        });
        
        usort($this->data, array($this, 'usort'));
    }

    /**
     * @return bool|MenuItemIterator
     */
    public function current()
    {
        /* @var $current \Zechim\AppBundle\Entity\Menu */
        $current = current($this->data); 
        
        if (false === $current || null === $current) {
            return false;
        }
        
        if ($current instanceof Menu) {
            $current = new MenuItemIterator($this->raw, $current, $this->pm);
        }

        return $this->data[$this->key()] = $current;
    }
}    