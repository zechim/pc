<?php

namespace Zechim\AppBundle\Service\Route;

use Symfony\Component\Routing\RouteCollection;
use Zechim\AppBundle\Service\Dispatcher\AbstractDispatcher;

class Loader
{
    /**
     * @var RouteCollection
     */
    private $routeCollection;

    public function __construct()
    {
        $this->routeCollection = new RouteCollection();
    }

    /**
     * @param AbstractDispatcher $dispatcher
     */
    public function addRouteCollectionByDispatcher(AbstractDispatcher $dispatcher)
    {
        $this->routeCollection->addCollection($dispatcher->createRouteCollection());
    }

    /**
     * @return RouteCollection
     */
    public function load()
    {
        return $this->routeCollection;
    }
}