<?php

namespace Zechim\AppBundle;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;
use Zechim\AppBundle\DependencyInjection\Compiler\DispatcherCompilerPass;

class ZechimAppBundle extends Bundle
{
    /**
     * {@inheritdoc}
     */
    public function build(ContainerBuilder $container)
    {
        $container->addCompilerPass(new DispatcherCompilerPass());
    }
}
