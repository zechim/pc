<?php

namespace Zechim\AppBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Zechim\AppBundle\Entity\UserAction;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Util\ClassUtils;
use Zechim\AppBundle\Entity\User;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;

class UserActionRepository extends EntityRepository
{
    /**
     * @param User $user
     * @return ArrayCollection|UserAction
     */
    public function getActionByUser(User $user)
    {
        $qb = $this->createQueryBuilder('userAction');
        $qb->select('userAction');

        $qb->join('userAction.userPermission', 'userPermission');
        $qb->join('userPermission.user', 'user');

        $qb->andWhere('user = :user');
        $qb->setParameter('user', $user->getId());

        $qb->getQuery()->useQueryCache(true);
        $qb->getQuery()->useResultCache(true, 3600, __METHOD__ . $user->getId());

        return new ArrayCollection($qb->getQuery()->getResult());
    }

    /**
     * @param $entity
     * @return UserAction|null
     */
    public function createEntityPermission($entity)
    {
        if (null !== $found = $this->findByEntity($entity)) {
            return $found;
        }

        $class = ClassUtils::getClass($entity);

        $userAction = new UserAction();
        $userAction->setCode(
            sprintf(
                '%s_%s',
                ClassUtils::newReflectionClass($class)->getShortName(),
                $entity->getId()
            )
        );
        $userAction->setName((string) $entity);
        $userAction->setReferenceClass($class);
        $userAction->setReferenceValue($entity->getId());

        $this->_em->persist($userAction);
        $this->_em->flush($userAction);

        $this->getUserPermissionRepository()->addUserActionOnAdminPermission($userAction);

        return $userAction;
    }

    /**
     * @param $entity
     * @return null|object|UserAction
     */
    protected function findByEntity($entity)
    {
        return $this->findOneBy(
            [
                'referenceClass' => ClassUtils::getClass($entity),
                'referenceValue' => $entity->getId()
            ]
        );
    }

    public function removeEntityPermission($entity)
    {
        $found = $this->findByEntity($entity);

        if (null === $found) {
            return null;
        }

        $this->_em->remove($found);
        $this->_em->flush($found);
    }

    public function filterByUserAction(QueryBuilder $queryBuilder, User $user, $referenceClass)
    {
        $queryBuilder->join(
            UserAction::class,
            'ua',
            Join::WITH,
            sprintf(
                "ua.referenceValue = b.id AND ua.referenceClass = '%s'",
                $referenceClass
            )
        );

        $queryBuilder->leftJoin('ua.userPermission', 'up');
        $queryBuilder->leftJoin('up.user', 'user');
        $queryBuilder->andWhere('user = :user');
        $queryBuilder->setParameter('user', $user);

        return $queryBuilder;
    }
}
