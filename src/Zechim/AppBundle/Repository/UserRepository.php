<?php

namespace Zechim\AppBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Zechim\AppBundle\Entity\User;
use Zechim\AppBundle\Service\Pagination\Factory;

/**
 * UserRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class UserRepository extends EntityRepository
{
    /**
     * @param $page
     * @param int $limit
     * @return \Zechim\AppBundle\Service\Pagination\Pagination
     */
    public function getPagination($page, $limit = 15)
    {
        return Factory::getPagination($this->createQueryBuilder('u'), $page, $limit);
    }

    /**
     * @param User $user
     * @param User $createdBy
     * @return User
     */
    public function create(User $user, User $createdBy)
    {
        $user->setUpdatedBy($createdBy);
        $user->setCreatedBy($createdBy);
        $user->setUpdatedAt(new \DateTime());
        $user->setCreatedAt(new \DateTime());

        $this->_em->persist($user);
        $this->_em->flush();

        return $user;
    }

    /**
     * @param User $user
     * @param User $updatedBy
     * @return User
     */
    public function edit(User $user, User $updatedBy)
    {
        $user->setUpdatedBy($updatedBy);
        $user->setUpdatedAt(new \DateTime());

        $this->_em->persist($user);
        $this->_em->flush();

        return $user;
    }

    /**
     * @param User $user
     * @param User $currentUser
     * @throws \Exception
     */
    public function delete(User $user, User $currentUser)
    {
        if ($user->getId() === $currentUser->getId()) {
            throw new \Exception('trans.message.cant.delete.own.user');
        }

        $this->_em->remove($user);
        $this->_em->flush();
    }
}
