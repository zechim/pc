<?php

namespace Zechim\AppBundle\Dispatcher;

use Symfony\Component\HttpFoundation\Request;
use Zechim\AppBundle\Form\UserPermissionType;
use Zechim\AppBundle\Service\Dispatcher\AbstractDispatcher;

class UserPermissionDispatcher extends AbstractDispatcher
{
    protected function createEditForm($entity, Request $request)
    {
        return $this->createForm(UserPermissionType::class, $entity);
    }

    protected function createCreateForm(Request $request)
    {
        return $this->createEditForm(null, $request);
    }
}
