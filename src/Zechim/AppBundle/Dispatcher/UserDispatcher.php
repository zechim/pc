<?php

namespace Zechim\AppBundle\Dispatcher;

use Symfony\Component\HttpFoundation\Request;
use Zechim\AppBundle\Form\UserType;
use Zechim\AppBundle\Service\Dispatcher\AbstractDispatcher;

class UserDispatcher extends AbstractDispatcher
{
    protected function createEditForm($entity, Request $request)
    {
        return $this->createForm(
            UserType::class,
            $entity,
            [
                'encoder' => $this->get('security.encoder_factory'),
                'translator' => $this->get('translator')
            ]
        );
    }

    protected function createCreateForm(Request $request)
    {
        return $this->createEditForm(null, $request);
    }
}