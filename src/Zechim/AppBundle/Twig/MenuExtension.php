<?php

namespace Zechim\AppBundle\Twig;

use Zechim\AppBundle\Entity\Menu;
use Zechim\AppBundle\Service\Menu\MenuBuilder;
use Zechim\AppBundle\Service\Menu\MenuCriteria;
use Zechim\AppBundle\Service\Menu\MenuItemIterator;
use Symfony\Component\Routing\RouterInterface;

class MenuExtension extends \Twig_Extension
{
    /**
     * @var MenuBuilder
     */
    protected $menuBuilder;

    /**
     * @var RouterInterface
     */
    protected $router;

    /**
     * MenuExtension constructor.
     * @param MenuBuilder $menuBuilder
     * @param RouterInterface $router
     */
    public function __construct(MenuBuilder $menuBuilder, RouterInterface $router)
    {
        $this->menuBuilder = $menuBuilder;
        $this->router = $router;
    }

    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('MENU_render', [$this, 'renderMenu'], ['needs_environment' => true, 'is_safe' => ['html']]),
            new \Twig_SimpleFunction('MENU_render_submenu', [$this, 'renderSubmenu'], ['needs_environment' => true, 'is_safe' => ['html']]),
            new \Twig_SimpleFunction('MENU_generate_route', [$this, 'generateRoute']),
        ];
    }

    /**
     * @param \Twig_Environment $twig
     * @return string
     */
    public function renderMenu(\Twig_Environment $twig)
    {
        return $twig->render(
            'ZechimAppBundle::menu.root.html.twig',
            [
                'menu' => $this->menuBuilder->build(new MenuCriteria())
            ]
        );
    }

    /**
     * @param \Twig_Environment $twig
     * @param MenuItemIterator $menu
     * @return string
     */
    public function renderSubmenu(\Twig_Environment $twig, MenuItemIterator $menu)
    {
        return $twig->render(
            'ZechimAppBundle::menu.submenu.html.twig',
            [
                'menu' => $menu
            ]
        );
    }

    /**
     * @param Menu $menu
     * @return string
     */
    public function generateRoute(Menu $menu)
    {
        if ('' === $route = trim($menu->getRoute())) {
            return '#';
        }

        return $this->router->generate($route);
    }

    public function getName()
    {
        return 'zechim_app_menu_extension';
    }

}