<?php

namespace Zechim\AppBundle\Twig;

use Zechim\AppBundle\Entity\Menu;
use Zechim\AppBundle\Service\Menu\MenuBuilder;
use Zechim\AppBundle\Service\Menu\MenuCriteria;
use Zechim\AppBundle\Service\Menu\MenuItemIterator;
use Symfony\Component\Routing\RouterInterface;
use Zechim\AppBundle\Service\PermissionManager\PermissionManager;

class PermissionExtension extends \Twig_Extension
{
    /**
     * @var MenuBuilder
     */
    protected $permissionManager;

    /**
     * @var RouterInterface
     */
    protected $router;

    public function __construct(PermissionManager $permissionManager)
    {
        $this->permissionManager = $permissionManager;
    }

    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('PERMISSION_is_granted', [$this, 'isGranted']),
        ];
    }

    /**
     * @param $role
     * @return bool
     */
    public function isGranted($role)
    {
        return true === $this->permissionManager->isGranted($role);
    }

    public function getName()
    {
        return 'zechim_app_permission_extension';
    }

}