<?php

namespace Zechim\AppBundle\EventListener;
 

use Symfony\Component\HttpFoundation\JsonResponse;
use Zechim\AppBundle\Exception\TransException;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\Translation\TranslatorInterface;

class ControllerExceptionListener
{
    /**
     * @var \Symfony\Bundle\FrameworkBundle\Templating\EngineInterface
     */
    protected $templating;
    
    /**
     * @var \Symfony\Component\Translation\TranslatorInterface
     */
    protected $translator;
    
    /**
     * @var string
     */
    protected $env;
    
    public function __construct(EngineInterface $templating, TranslatorInterface $translator, $env)
    {
        $this->templating = $templating;
        $this->translator = $translator;
        $this->env = $env;
    }
    
    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        if ('dev' === $this->env && false === $event->getException() instanceof TransException) {
            return;
        }

        if (true === $event->getException() instanceof TransException) {
            $message = $event->getException()->trans($this->translator);

        } else {
            $message = $event->getException()->getMessage();

        }

        if ('security.firewall.map.context.api' === $event->getRequest()->get('_firewall_context')) {
            $response = new JsonResponse(
                [
                    'error' => true,
                    'message' => $message
                ],
                Response::HTTP_INTERNAL_SERVER_ERROR
            );

        } else {
            $response = new Response();
            $response->setContent(
                $this->templating->render(
                    'ZechimAppBundle:Default:error.html.twig',
                    ['message' => $message]
                )
            );
        }
        

        
        $event->setResponse($response);
    }
}
