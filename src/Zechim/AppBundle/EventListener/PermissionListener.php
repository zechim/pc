<?php

namespace Zechim\AppBundle\EventListener;

use Zechim\AppBundle\Entity\Menu;
use Zechim\AppBundle\Entity\User;
use Zechim\AppBundle\Exception\TransException;
use Zechim\AppBundle\Service\Menu\MenuRequestFetcher;
use Zechim\AppBundle\Service\PermissionManager\PermissionManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;

class PermissionListener
{
    /**
     * @var \Zechim\AppBundle\Service\PermissionManager\PermissionManager
     */
    protected $pm;
    
    /**
     * @var \Doctrine\ORM\EntityManagerInterface
     */
    protected $em;
    
    /**
     * @var \Zechim\AppBundle\Service\Menu\MenuRequestFetcher
     */
    protected $menuRequestFetcher;

    /**
     * PermissionListener constructor.
     * @param PermissionManager $pm
     * @param EntityManagerInterface $em
     * @param MenuRequestFetcher $menuRequestFetcher
     */
    public function __construct(PermissionManager $pm, EntityManagerInterface $em, MenuRequestFetcher $menuRequestFetcher)
    {
        $this->pm = $pm;
        $this->em = $em;
        $this->menuRequestFetcher = $menuRequestFetcher;
    }
    
    public function onKernelController(FilterControllerEvent $event)
    {
        if (true === $this->menuRequestFetcher->getIsSkipped()) {
            return;
        }

        if ($this->pm->getUser()->getToken() !== $event->getRequest()->getSession()->get(AuthenticationListener::TOKEN_SESSION_KEY, 0)) {
            throw new TransException('trans.message.you_were_disconnect_by_another_session');
        }

        if (false === ($this->menuRequestFetcher->getMenu() instanceof Menu)) {
            $ex = new TransException('trans.message.menu_route_not_found');
            $ex->setParameters(['{{ route }}' => $this->menuRequestFetcher->getRoute()]);

            throw $ex;
        }

        if (null !== $this->menuRequestFetcher->getMenu()->getUserAction() &&
            false === $this->pm->hasAction($this->menuRequestFetcher->getMenu()->getUserAction())) {

            $ex = new TransException('trans.message.user_has_no_menu_permission');
            $ex->setParameters(['{{ menu }}' => $this->menuRequestFetcher->getPageName()]);

            throw $ex;
        }
    }
}
