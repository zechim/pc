<?php

namespace Zechim\ApiBundle\Service;

use Symfony\Component\Security\Http\Authentication\AuthenticationFailureHandlerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use Symfony\Component\Security\Core\Authentication\Token\PreAuthenticatedToken;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\User\UserChecker;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Http\Authentication\SimplePreAuthenticatorInterface;
use Symfony\Component\Translation\TranslatorInterface;

class Authenticator implements SimplePreAuthenticatorInterface, AuthenticationFailureHandlerInterface
{
    const PROVIDER_KEY = 'api';

    /**
     * @var TranslatorInterface
     */
    protected $translator;

    /**
     * @var UserProvider
     */
    protected $userProvider;

    /**
     * Authenticator constructor.
     * @param TranslatorInterface $translator
     * @param UserProvider $userProvider
     */
    public function __construct(TranslatorInterface $translator, UserProvider $userProvider)
    {
        $this->translator = $translator;
        $this->userProvider = $userProvider;
    }

    /**
     * @param Request $request
     * @param $providerKey
     * @return PreAuthenticatedToken
     */
    public function createToken(Request $request, $providerKey)
    {
        $token = trim($request->headers->get('app-token'));

        if (true === empty($token)) {
            throw new BadCredentialsException($this->translator->trans('trans.api.message.token.not.informed'));
        }

        return new PreAuthenticatedToken($token, null, $providerKey);
    }

    /**
     * @param TokenInterface $token
     * @param UserProviderInterface $userProvider
     * @param $providerKey
     * @return PreAuthenticatedToken
     */
    public function authenticateToken(TokenInterface $token, UserProviderInterface $userProvider, $providerKey)
    {
        $userToken = $this->userProvider->loadUserByToken($token->getUsername());

        $checker = new UserChecker();
        $checker->checkPreAuth($userToken->getUser());
        $checker->checkPostAuth($userToken->getUser());

        if (false === $userToken->getIsActive()) {
            throw new BadCredentialsException($this->translator->trans('trans.api.message.token.not.active'));
        }

        $expiresAt = $userToken->getExpiresAt();

        if (null !== $expiresAt && $expiresAt < new \DateTime()) {
            throw new BadCredentialsException($this->translator->trans('trans.api.message.token.is.expired'));
        }
        
        return new PreAuthenticatedToken($userToken->getUser(), null, $providerKey, $userToken->getUser()->getRoles());
    }

    /**
     * @param TokenInterface $token
     * @param $providerKey
     * @return bool
     */
    public function supportsToken(TokenInterface $token, $providerKey)
    {
        return 'Symfony\Component\Security\Core\Authentication\Token\PreAuthenticatedToken' === get_class($token) && self::PROVIDER_KEY === $token->getProviderKey();
    }

    /**
     * @param Request $request
     * @param AuthenticationException $exception
     * @return JsonResponse
     */
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        return new JsonResponse(
            [
                'error' => true,
                'message' => $exception->getMessage()
            ],
            Response::HTTP_INTERNAL_SERVER_ERROR
        );
    }
}
