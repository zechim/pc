<?php

namespace Zechim\ApiBundle\Dispatcher;

use Symfony\Component\HttpFoundation\Request;
use Zechim\ApiBundle\Form\UserTokenType;
use Zechim\AppBundle\Service\Dispatcher\AbstractDispatcher;

class UserTokenDispatcher extends AbstractDispatcher
{
    protected function createEditForm($entity, Request $request)
    {
        return $this->createForm(UserTokenType::class, $entity);
    }

    protected function createCreateForm(Request $request)
    {
        return $this->createEditForm(null, $request);
    }
}
