<?php

namespace Zechim\ApiBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Validator\Constraints\NotBlank;
use Zechim\ApiBundle\Entity\UserToken;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Zechim\AppBundle\Entity\User;
use Zechim\AppBundle\Form\Transform\DateTimeTransform;

class UserTokenType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('user', EntityType::class, [
            'label' => 'trans.label.user',
            'class' => User::class,
            'attr' => [
                'class' => 'form-control',
            ],
        ]);

        $builder->add('isActive', ChoiceType::class, [
            'label' => 'trans.label.isActive',
            'expanded' => true,
            'choices' => [
                'trans.message.yes' => 1,
                'trans.message.no' => 0
            ],
            'attr' => [
                'class' => 'form-control',
            ],
            'constraints' => [new NotBlank()]
        ]);

        $builder->add('token', null, [
            'label' => 'trans.label.token',
            'attr' => [
                'class' => 'form-control',
            ],
            'constraints' => [new NotBlank()]
        ]);

        $builder->add(
            $builder->create('expiresAt', null, [
                'label' => 'trans.label.expiresAt',
                'attr' => ['class' => 'form-control date'],
                'widget' => 'single_text',
                'format' => 'dd/MM/yyyy'
            ])->addModelTransformer(new DateTimeTransform())
        );
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => UserToken::class,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'zechim_api_user_token';
    }
}
